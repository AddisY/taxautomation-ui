/**
 * Created by Yonas on 2/19/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.TaxableServiceSummary')
        .controller('TaxableServiceSummaryAddCtrl',TaxableServiceSummaryAddCtrl);

    function TaxableServiceSummaryAddCtrl($scope, $state,$stateParams, $rootScope,TaxableServiceSummaryService){
        var vm = this;
        vm.companyID = "1";
        vm.index = 0;
        vm.serviceItems = [];
        vm.addItem = addItem;
        vm.removeItem = removeItem;
        vm.serviceSummary = serviceSummary;
        vm.serviceTotal = serviceTotal;


        function addItem(){
            debugger;
            vm.serviceItems.push({
                index: vm.index+=1,
                item_name: "",
                item_price: "",
                company_name: "",
                tin_number: "",
                mrc_code: "",
                company_id: "",
                bill_code: "",
                bill_date: ""
            });
        };

        function removeItem(){
            var newDataList=[];
            vm.selectedAll = false;
            angular.forEach(vm.serviceItems, function(selected){
                if(!selected.selected){
                    newDataList.push(selected);
                }
            });
            vm.serviceItems = newDataList;
            vm.index = 0;
        };

        function serviceTotal(){
            vm.subTotal = 0;
            vm.vatTotal = 0;
            vm.withholdTotal = 0;
            for(var i=0;i<vm.serviceItems.length;i++){
                vm.subTotal += (vm.serviceItems[i].item_price);
                vm.vatTotal += (vm.serviceItems[i].item_price * 0.15);
                vm.withholdTotal += (vm.serviceItems[i].item_price * 0.02);
            }
            debugger;

        }

        function serviceSummary(){
            debugger;
            vm.itemsList = [];

            for(var i=0; i<vm.serviceItems.length; i++){
                vm.itemsList.push({
                    company_name: vm.serviceItems[i].company_name,
                    tin_number: vm.serviceItems[i].tin_number,
                    mrc_code: vm.serviceItems[i].mrc_code,
                    item_name: vm.serviceItems[i].item_name,
                    item_price: vm.serviceItems[i].item_price,
                    company_id: vm.companyID,
                    bill_code: vm.serviceItems[i].bill_code,
                    bill_date:vm.year + "-" + vm.month + "-" + vm.serviceItems[i].bill_date,
                    withholding_date: vm.year + "-" + vm.month + "-" + vm.serviceItems[i].withholding_date,
                    withholding_code: vm.serviceItems[i].withholding_code,
                    item_type: "local;",
                    withholding_tax_number : "12121",
                    purchase_type: "service"
                });
            }
            vm.itemsData = {locapurchase:  vm.itemsList};

            TaxableServiceSummaryService.createServiceSummary(vm.itemsData).then(function(response){
                $state.go('main.TaxableServiceSummaryUndeclared');
            });
        };

    }
})();