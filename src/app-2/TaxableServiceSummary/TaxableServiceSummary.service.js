/**
 * Created by Yonas on 2/19/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.TaxableServiceSummary')
        .service('TaxableServiceSummaryService', TaxableServiceSummaryService);
    /*@ngNoInject*/
    function TaxableServiceSummaryService(TokenRestangular, $rootScope) {

        var service = {
            getSummaryHistory: getSummaryHistory,
            getSummaryDetail: getSummaryDetail,
            createServiceSummary: createServiceSummary,
            editServiceSummary: editServiceSummary,
            deleteServiceSummary: deleteServiceSummary
        };
        return service;

        function getSummaryHistory(){
            debugger;
            return TokenRestangular.all('localpurchase').customGET('');
        }

        function getSummaryDetail(date,uid){
            debugger;
            return TokenRestangular.all('localpurchase/'+date+'/'+uid).customGET('');
        }

        function createServiceSummary(data){
            debugger;
            return TokenRestangular.all('localpurchase').customPOST(data);
        }

        function editServiceSummary(data){
            debugger;
            return TokenRestangular.all('localpurchase').customPUT(data);
        }

        function deleteServiceSummary(itemID){
            debugger;
            return TokenRestangular.all('localpurchase/'+itemID).customDELETE()
        }
    }

})();