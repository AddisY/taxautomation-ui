/**
 * Created by Yonas on 3/20/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.allowance')
        .controller('AllowanceUndeclaredCtrl',AllowanceUndeclaredCtrl);
    function AllowanceUndeclaredCtrl($scope, $state, $rootScope,AllowanceService){
        var vm = this;
        vm.uid = '1';
        vm.allowance = [];
        var flags = [];
        vm.date = [];

        getAllowance();

        function getAllowance(){
            AllowanceService.getAllowanceHistory(vm.uid).then(function(response){
                for(var i=0; i<response.employe_informations.length;i++){
                    for(var j=0; j<response.employe_informations[i].allowance.length;j++){
                        if(response.employe_informations[i].allowance[j].status == '0'){
                            vm.date[j] = response.employe_informations[i].allowance[j].end.split("-")[0]+""+ response.employe_informations[i].allowance[j].end.split("-")[1];
                            if( flags[vm.date[j]]) continue;
                            flags[vm.date[j]] = true;
                            vm.allowance.push(response.employe_informations[i].allowance[j]);
                        }
                    }
                }
            });
        }
    }
})();