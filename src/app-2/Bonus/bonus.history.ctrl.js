/**
 * Created by Yonas on 3/15/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.bonus')
        .controller('BonusHistoryCtrl',BonusHistoryCtrl);

    function BonusHistoryCtrl(BonusService){
        var vm = this;
        vm.uid = '1';
        vm.bonus = [];
        var flags = [];
        vm.date = [];
        getBonusRecord();

        function getBonusRecord(){
            debugger;
            BonusService.getBonus(vm.uid).then(function(response){
               for(var i=0; i<response.length;i++){
                   for(var j=0; j<response[i].bonus.length;j++){
                       if(response[i].bonus[j].status == '1'){
                           vm.date[j] = response[i].bonus[j].pay_day.split("-")[0]+""+response[i].bonus[j].pay_day.split("-")[1];
                           if( flags[vm.date[j]]) continue;
                           flags[vm.date[j]] = true;
                           vm.bonus.push(response[i].bonus[j]);
                       }
                   }
               }
            });
        }
    }
})();