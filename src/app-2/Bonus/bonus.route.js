/**
 * Created by Yonas on 3/15/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.bonus')
        .config(moduleConfig);
    /*@ngNoInject*/
    function moduleConfig($stateProvider) {
        $stateProvider
            .state('main.BonusHistory', {
                url: "/BonusHistory",
                templateUrl: "app/Bonus/bonus.history.html",
                controller: 'BonusHistoryCtrl',
                controllerAs: 'vm'
            })
            .state('main.BonusDetail', {
                url: "/BonusDetail/{uid}/{date}",
                templateUrl: "app/Bonus/bonus.detail.html",
                controller: 'BonusDetailCtrl',
                controllerAs: 'vm'
            })
            .state('main.BonusUndeclared', {
                url: "/BonusUndeclared",
                templateUrl: "app/Bonus/bonus.undeclared.html",
                controller: 'BonusUndeclaredCtrl',
                controllerAs: 'vm'
            })
            .state('main.BonusEdit', {
                url: "/BonusEdit/{uid}/{date}",
                templateUrl: "app/Bonus/bonus.edit.html",
                controller: 'BonusEditCtrl',
                controllerAs: 'vm'
            })
            .state('main.BonusAdd', {
                url: "/BonusAdd",
                templateUrl: "app/Bonus/bonus.add.html",
                controller: 'BonusAddCtrl',
                controllerAs: 'vm'
            })

    }

})();
