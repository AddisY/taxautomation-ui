/**
 * Created by Yonas on 3/13/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.pension')
        .controller('pensionDeclarationCtrl',pensionDeclarationCtrl);
    function pensionDeclarationCtrl(PensionService){
        var vm = this;
        vm.uid = "1";
        vm.company = {};
        vm.employees = [];
        vm.terminated = [];
        vm.total1 = 0;
        vm.total2 = 0;
        vm.employeeCount = "";
        vm.totalSalary = "";
        vm.getPension = getPension;

        function getPension(){
           debugger;
            var date = vm.year+"-"+vm.month+"-01";
            PensionService.getPension(vm.uid,date).then(function(response){
                vm.company = response.company_Info;
                vm.totalSalary = response.total_salary;
                vm.employeeCount = response.numberof_employe;

                for(var i=0;i<response.employe_info.length;i++){
                    vm.employees.push(response.employe_info[i]);
                    vm.total1 += (vm.employees[i].salary*0.07);
                    vm.total2 += (vm.employees[i].salary*0.11);
                }

                for(var j=0;j<response.terminated.length;j++){
                    vm.terminated.push(response.terminated[j]);
                }

            });
        }
    }
})();