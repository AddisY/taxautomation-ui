/**
 * Created by Yonas on 3/13/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.pension')
        .config(moduleConfig);
    /*@ngNoInject*/
    function moduleConfig($stateProvider) {
        $stateProvider
            .state('main.pensionDeclaration',{
                url: "/pensionDeclaration",
                templateUrl: "app/PensionDeclaration/pension.declaration.html",
                controller: 'pensionDeclarationCtrl',
                controllerAs: 'vm'
            })
    }

})();