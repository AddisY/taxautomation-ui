/**
 * Created by Yonas on 2/20/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.VatDeclaration')
        .service('VatDeclarationService', VatDeclarationService);
    /*@ngNoInject*/
    function VatDeclarationService(TokenRestangular, $rootScope) {

        var service = {
            getVatReport: getVatReport,
            vatDeclaration: vatDeclaration
        };
        return service;

        function getVatReport(data,uid){
            debugger;
            return TokenRestangular.all('vat_report/'+data+'/'+uid).customGET('');
        }

        function vatDeclaration(date,uid){
            debugger;
            return TokenRestangular.all('updatstatus/'+date+'/'+uid).customPUT('');
        }
    }

})();