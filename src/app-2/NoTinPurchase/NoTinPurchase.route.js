/**
 * Created by Yonas on 2/20/2017.
 */
(function (){
    'use strict';
    angular
        .module('app.NoTinPurchase')
        .config(moduleConfig);

    function moduleConfig($stateProvider){
        $stateProvider
            .state('main.NoTinPurchaseHistory',{
                url: "/NoTinPurchaseHistory",
                templateUrl: "app/NoTinPurchase/NoTinPurchaseHistory.html",
                controller: 'NoTinPurchaseHistoryCtrl',
                controllerAs: 'vm'
            })
            .state('main.NoTinPurchaseDetail',{
                url: "/NoTinPurchaseDetail/{uid}/{date}",
                templateUrl: "app/NoTinPurchase/NoTinPurchaseDetail.html",
                controller: 'NoTinPurchaseDetailCtrl',
                controllerAs: 'vm'
            })
            .state('main.NoTinPurchaseUndeclared',{
                url: "/NoTinPurchaseUndeclared",
                templateUrl: "app/NoTinPurchase/NoTinPurchaseUndeclared.html",
                controller: 'NoTinPurchaseUndeclaredCtrl',
                controllerAs: 'vm'
            })
            .state('main.NoTinPurchaseEdit',{
                url: "/NoTinPurchaseEdit/{uid}/{date}",
                templateUrl: "app/NoTinPurchase/NoTinPurchaseEdit.html",
                controller: 'NoTinPurchaseEditCtrl',
                controllerAs: 'vm'
            })
            .state('main.NoTinPurchaseAdd',{
                url: "/NoTinPurchaseAdd",
                templateUrl: "app/NoTinPurchase/NoTinPurchaseAdd.html",
                controller: 'NoTinPurchaseAddCtrl',
                controllerAs: 'vm'
            })
    }
})();