/**
 * Created by Yonas on 2/19/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.NoTinPurchase')
        .controller('NoTinPurchaseEditCtrl',NoTinPurchaseEditCtrl);

    function NoTinPurchaseEditCtrl($scope, $stateParams, $state, $rootScope,NoTinPurchaseService){
        var vm = this;
        vm.date = $stateParams.date;
        vm.uid = $stateParams.uid;
        vm.purchasedItems = [];
        vm.addItem = addItem;
        vm.removeItem = removeItem;
        vm.purchaseTotal = purchaseTotal;
        vm.purchaseSummary = purchaseSummary;
        vm.year = vm.date.split("-")[0];
        vm.month = vm.date.split("-")[1];

        getSummaryDetail();

        function getSummaryDetail(){
            debugger;
            NoTinPurchaseService.getSummaryDetail(vm.date,vm.uid).then(function(response){
                for(var i=0; i<response.data.length; i++){
                    vm.purchasedItems.push({
                        id: response.data[i].id,
                        company_name:response.data[i].company_name,
                        item_price:response.data[i].item_price,
                        company_id:vm.uid,
                        zone:response.data[i].zone,
                        sub_city:response.data[i].sub_city,
                        purchase_type:response.data[i].purchase_type,
                        woreda:response.data[i].woreda,
                        house_number:response.data[i].house_number,
                        voucher_number:response.data[i].voucher_number,
                        voucher_date:response.data[i].voucher_date.split("-")[2] * 1
                    });
                }
            });
        }

        function addItem(){
            debugger;
            vm.purchasedItems.push({
                id: "-",
                company_name:"",
                item_price:"",
                company_id:"",
                zone:"",
                sub_city:"",
                purchase_type:"",
                woreda:"",
                house_number:"",
                voucher_number:"",
                voucher_date:""
            });
        }

        function removeItem(){
            var newDataList=[];
            vm.selectedAll = false;
            angular.forEach(vm.purchasedItems, function(selected){
                if(!selected.selected){
                    newDataList.push(selected);
                }
            });
            vm.purchasedItems = newDataList;
            vm.index = 0;
        }

        function purchaseTotal(){
            vm.subTotal = 0;
            vm.withholdTotal = 0;
            for(var i=0;i<vm.purchasedItems.length;i++){
                vm.subTotal += (vm.purchasedItems[i].item_price * 1);
                vm.withholdTotal += (vm.purchasedItems[i].item_price * 0.02);
            }
            debugger;

        }

        function purchaseSummary(){
            debugger;
            vm.itemsList = [];
            vm.editList = [];
            vm.createList = [];

            for(var i=0; i<vm.purchasedItems.length; i++){
                vm.itemsList.push({
                    id: vm.purchasedItems[i].id,
                    company_name:vm.purchasedItems[i].company_name,
                    item_price: vm.purchasedItems[i].item_price,
                    company_id: vm.uid,
                    zone: vm.purchasedItems[i].zone,
                    sub_city: vm.purchasedItems[i].sub_city,
                    purchase_type: vm.purchasedItems[i].purchase_type,
                    woreda: vm.purchasedItems[i].woreda,
                    house_number: vm.purchasedItems[i].house_number,
                    voucher_number: vm.purchasedItems[i].voucher_number,
                    voucher_date: vm.year + "-" +vm.month+"-"+vm.purchasedItems[i].voucher_date
                });
            }

            for(var i=0; i<vm.itemsList.length; i++){
                if(vm.itemsList[i].id != '-'){
                    vm.editList.push(vm.itemsList[i]);
                }
                else{
                    vm.createList.push({
                        company_name:vm.purchasedItems[i].company_name,
                        item_price: vm.purchasedItems[i].item_price,
                        company_id: vm.uid,
                        zone: vm.purchasedItems[i].zone,
                        sub_city: vm.purchasedItems[i].sub_city,
                        purchase_type: vm.purchasedItems[i].purchase_type,
                        woreda: vm.purchasedItems[i].woreda,
                        house_number: vm.purchasedItems[i].house_number,
                        voucher_number: vm.purchasedItems[i].voucher_number,
                        voucher_date: vm.year +"-"+vm.month+"-"+vm.purchasedItems[i].voucher_date
                    });
                }
            }
            vm.editData = {nonregpurchase:  vm.editList};
            vm.createData = {nonregpurchase: vm.createList};

            NoTinPurchaseService.createPurchaseSummary(vm.createData).then(function(response){
            });
            NoTinPurchaseService.editPurchaseSummary(vm.editData).then(function(response){
            });
            $state.go('main.NoTinPurchaseUndeclared');
        }
    }
})();
