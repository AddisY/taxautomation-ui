(function () {
    'use strict';

    angular.module('app', ['app.core','app.auth','app.SalesSummary','app.TaxablePurchaseSummary',
        'app.TaxableServiceSummary','app.ForeignPurchaseSummary','app.NoTinPurchase','app.VatDeclaration',
    'app.WithholdDeclaration','app.employees','app.overtime','app.bonus','app.leave','app.allowance','app.severance',
        'app.pension','app.payroll'])
        .constant("appConstant", {
            "localApi": "http://localhost:8000/api/",
            "imagePath": "localhost:8000/",
            "grant_type": "client_credentials",
            "client_id": "$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD",
            "client_secret": "$2y$10$9OqJjxC9qZKAB2L.nO7hVOPY0436eU1CD"
        })
        .factory("TokenRestangular", tokenRestangular);

    /*@ngNoInject*/
    function tokenRestangular(Restangular, appConstant) {
        /*@ngNoInject*/
        return Restangular.withConfig(function (RestangularConfigurer) {
            RestangularConfigurer.setDefaultHeaders({Authorization: 'Bearer ' + localStorage.getItem('automation_token')});
            RestangularConfigurer.setBaseUrl(appConstant.localApi);
        });

    }

})();
