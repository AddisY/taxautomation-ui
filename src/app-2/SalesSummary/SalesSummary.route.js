/**
 * Created by Yonas on 2/16/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.SalesSummary')
        .config(moduleConfig);
    /*@ngNoInject*/
    function moduleConfig($stateProvider) {
        $stateProvider
            .state('main.SalesSummaryHistory', {
                url: "/SalesSummaryHistory",
                templateUrl: "app/SalesSummary/SalesSummaryHistory.html",
                controller: 'SalesSummaryHistoryCtrl',
                controllerAs: 'vm'
            })
            .state('main.SalesSummaryDetail',{
                url: "/SalesSummaryDetail/{uid}/{date}",
                templateUrl: "app/SalesSummary/SalesSummaryDetail.html",
                controller: 'SalesSummaryDetailCtrl',
                controllerAs: 'vm'
            })
            .state('main.SalesSummaryUndeclared',{
                url: "/SalesSummaryUndeclared",
                templateUrl: "app/SalesSummary/SalesSummaryUndeclared.html",
                controller: 'SalesSummaryUndeclaredCtrl',
                controllerAs: 'vm'
            })
            .state('main.SalesSummaryEdit',{
                url: "/SalesSummaryEdit/{uid}/{date}",
                templateUrl: "app/SalesSummary/SalesSummaryEdit.html",
                controller: 'SalesSummaryEditCtrl',
                controllerAs: 'vm'
            })
            .state('main.SalesSummaryAdd',{
                url: "/SalesSummaryAdd",
                templateUrl: "app/SalesSummary/SalesSummaryAdd.html",
                controller: 'SalesSummaryAddCtrl',
                controllerAs: 'vm'
            })
    }

})();
