/**
 * Created by Yonas on 12/24/2016.
 */

(function(){
    'use strict';
    angular
        .module('app.SalesSummary')
        .controller('SalesSummaryAddCtrl',SalesSummaryAddCtrl);

    function SalesSummaryAddCtrl($scope, $state, $rootScope,SalesSummaryService){
        var vm = this;
        vm.companyID = "1";
        vm.index = 0;
        vm.salesItems = [];
        vm.addItem = addItem;
        vm.removeItem = removeItem;
        vm.salesSummary = salesSummary;
        vm.salesTotal = salesTotal;


        function addItem(){
            debugger;
            vm.salesItems.push({
                index: vm.index+=1,
                item_name: "",
                item_price: "",
                company_id: "",
                bill_code: "",
                bill_date: ""
            });
        }

        function removeItem(){
            var newDataList=[];
            vm.selectedAll = false;
            angular.forEach(vm.salesItems, function(selected){
                if(!selected.selected){
                    newDataList.push(selected);
                }
            });
            vm.salesItems = newDataList;
            vm.index = 0;
        }

        function salesTotal(){
            vm.subTotal = 0;
            vm.vatTotal = 0;
            for(var i=0;i<vm.salesItems.length;i++){
                vm.subTotal += (vm.salesItems[i].item_price);
                vm.vatTotal += (vm.salesItems[i].item_price * 0.15);
            }
            debugger;

        }

        function salesSummary(){
            debugger;
            vm.itemsList = [];

            for(var i=0; i<vm.salesItems.length; i++){
                vm.itemsList.push({
                    item_name: vm.salesItems[i].item_name,
                    item_price: vm.salesItems[i].item_price,
                    company_id: vm.companyID,
                    bill_code: vm.salesItems[i].bill_code,
                    bill_date:vm.year + "-" + vm.month + "-" + vm.salesItems[i].bill_date
                });
            }
            vm.itemsData = {localsales:  vm.itemsList};

            SalesSummaryService.createSalesSummary(vm.itemsData).then(function(response){
                $state.go('main.SalesSummaryHistory');
             });
      }

    }
})();
