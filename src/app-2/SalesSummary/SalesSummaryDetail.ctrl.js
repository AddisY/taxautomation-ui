/**
 * Created by Yonas on 12/24/2016.
 */

(function(){
    'use strict';
    angular
        .module('app.SalesSummary')
        .controller('SalesSummaryDetailCtrl',SalesSummaryDetailCtrl);

    function SalesSummaryDetailCtrl($scope, $stateParams, $rootScope,SalesSummaryService){
        var vm = this;
        vm.date = $stateParams.date;
        vm.uid = $stateParams.uid;
        getSummaryDetail();

        function getSummaryDetail(){
            debugger;
            vm.salesSummary = [];
            vm.salesTotal = 0;
            vm.salesVAT = 0;
            var vat = 0.15;
            vm.year = '';
            vm.month = '';
            SalesSummaryService.getSummaryDetail(vm.date,vm.uid).then(function(response){
                for(var i=0; i<response.data.length; i++){
                    vm.salesSummary.push(response.data[i]);
                    vm.salesTotal += vm.salesSummary[i].item_price;
                    vm.salesVAT += (vm.salesSummary[i].item_price * vat);
                }
                vm.year = vm.salesSummary[0].bill_date.split("-")[0];
                vm.month = vm.salesSummary[0].bill_date.split("-")[1];
            });
        }


    }
})();
