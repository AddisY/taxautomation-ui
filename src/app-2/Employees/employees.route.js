/**
 * Created by Yonas on 3/10/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.employees')
        .config(moduleConfig);
    /*@ngNoInject*/
    function moduleConfig($stateProvider, $urlRouterProvider, RestangularProvider) {

        $stateProvider
            .state('main.employeesList', {
                url: "/employeesList",
                templateUrl: "app/Employees/employees.list.html",
                controller: 'EmployeesListCtrl',
                controllerAs: 'vm'
            })
            .state('main.employeeDetail', {
                url: "/employeesDetail/{employeeID}",
                templateUrl: "app/Employees/employee.detail.html",
                controller: 'EmployeeDetailCtrl',
                controllerAs: 'vm'
            })
            .state('main.employeeAdd', {
                url: "/employeeAdd",
                templateUrl: "app/Employees/employee.add.html",
                controller: 'EmployeeAddCtrl',
                controllerAs: 'vm'
            })

    }

})();
