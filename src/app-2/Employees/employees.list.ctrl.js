/**
 * Created by Yonas on 3/10/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.employees')
        .controller('EmployeesListCtrl',EmployeesListCtrl);

    function EmployeesListCtrl($scope, $state, $rootScope,EmployeesService){
        var vm = this;
        vm.uid = '1';
        vm.employees = [];
        getEmployeeList();
        vm.deleteEmployee = deleteEmployee;

        function getEmployeeList(){
            debugger;
            EmployeesService.getEmployeeList(vm.uid).then(function(response){
                for(var i=0; i<response.employe_informations.length; i++){
                    vm.employees.push(response.employe_informations[i]);
                }
            });
        }

        function deleteEmployee(employeeID){
            debugger;
            window.alert("Are you sure?");
            EmployeesService.deleteEmployee(employeeID).then(function(){
                $state.reload();
            });

        }
    }

})();