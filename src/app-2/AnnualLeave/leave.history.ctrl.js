/**
 * Created by Yonas on 3/15/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.leave')
        .controller('LeaveRecordCtrl',LeaveRecordCtrl);
    function LeaveRecordCtrl($scope, $state, $rootScope,LeaveService){
        var vm = this;
        vm.uid = '1';
        vm.leave = [];
        var flags = [];
        vm.date = [];

        getLeaveRecord();

        function getLeaveRecord(){
           debugger;
            LeaveService.getLeaveRecord(vm.uid).then(function(response){
               for(var i=0; i<response.length;i++){
                  for(var j=0; j<response[i].leave.length;j++){
                      if(response[i].leave[j].status == '0'){
                          vm.date[j] = response[i].leave[j].issued_date.split("-")[0] + "" +response[i].leave[j].issued_date.split("-")[1];
                          if( flags[vm.date[j]]) continue;
                          flags[vm.date[j]] = true;
                          vm.leave.push(response[i].leave[j]);
                      }
                  }
               }
            });
        }
    }
})();