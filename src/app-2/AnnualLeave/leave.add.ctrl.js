/**
 * Created by Yonas on 3/15/2017.
 */
(function (){
    'use strict';
    angular
        .module('app.leave')
        .controller('LeaveAddCtrl',LeaveAddCtrl);
    function LeaveAddCtrl($scope, $state, $rootScope,LeaveService){
        var vm = this;
        vm.companyID = '1';
        vm.employeeList = [];
        vm.employee = [];
        vm.leaveList = [];
        vm.leaveData = {};
        getEmployees();
        vm.addItem = addItem;
        vm.removeItem = removeItem;
        vm.addLeave = addLeave;

        function getEmployees(){
            debugger;
            LeaveService.getEmployeeList(vm.companyID).then(function(response){
                for(var i=0; i<response.employe_informations.length; i++){
                    vm.employeeList.push(response.employe_informations[i]);
                }
            });
        }

        function addItem(){
           vm.employee.push({
              id:"",
              leaveDate:'',
              leaveMonths:"",
              leaveDays:"",
              accumulatedDays:""
           });
        }

        function removeItem(){
            var newDataList=[];
            vm.selectedAll = false;
            angular.forEach(vm.employee, function(selected){
                if(!selected.selected){
                    newDataList.push(selected);
                }
            });
            vm.employee = newDataList;
            vm.index = 0;
        }

        function addLeave(){
            debugger;
            for(var i=0; i<vm.employee.length; i++){
                vm.leaveList.push({
                    employe_id:vm.employee[i].id,
                    leave_date: vm.leaveDays,
                    annual_leave:vm.leaveDays,
                    working_date:vm.workingDays,
                    served_leave_period: vm.employee[i].leaveMonths + "-" + vm.employee[i].leaveDays,
                    issued_date: vm.year + "-" + vm.month + "-" +vm.employee[i].issuedDate,
                    leave_accumulated: vm.employee[i].accumlatedDays
                });
            }

            vm.leaveData = {leave:vm.leaveList};
            LeaveService.createLeave(vm.leaveData).then(function(){
                $state.go('main.overtimeUndeclared');
            });

        }
    }
})();