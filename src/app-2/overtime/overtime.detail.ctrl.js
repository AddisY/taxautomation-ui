/**
 * Created by Yonas on 3/14/2017.
 */
(function (){
    'use strict';
    angular
        .module('app.overtime')
        .controller('OvertimeDetailCtrl',OvertimeDetailCtrl);

    function OvertimeDetailCtrl($scope, $stateParams, $rootScope,OvertimeService){
        var vm = this;
        vm.uid = $stateParams.uid;
        vm.date = $stateParams.date;
        vm.employee = [{
            name: "",
            department: "",
            basic_salary: "",
            hourly_rate: "",
            before_four: "",
            after_four: "",
            onweekend: "",
            onholiday: "",
            total: ""
        }];

        getOvertime();

        function getOvertime(){
            debugger;
            OvertimeService.getOvertimeDetail(vm.uid,vm.date).then(function(response){
                for(var i=0; i<response.length;i++){
                    for(var j=0; j<response[i].overtime.length; j++){
                        vm.employee.push({
                            name: response[i].employe.name,
                            department: response[i].employe.department,
                            basic_salary: response[i].overtime[j].basic_salary,
                            hourly_rate: response[i].overtime[j].hourly_rate,
                            before_four: response[i].overtime[j].before_four,
                            after_four: response[i].overtime[j].after_four,
                            onweekend: response[i].overtime[j].onweekend,
                            onholiday: response[i].overtime[j].onpublicHoliday,
                            total: response[i].overtime[j].before_four + response[i].overtime[j].after_four + response[i].overtime[j].onweekend + response[i].overtime[j].onpublicHoliday
                        });
                    }
                }
            });
        }



    }
})();