/**
 * Created by Yonas on 3/14/2017.
 */
(function (){
    'use strict';
    angular
        .module('app.overtime')
        .controller('OvertimeEditCtrl',OvertimeEditCtrl);

    function OvertimeEditCtrl($state, $stateParams, $rootScope,OvertimeService){
        var vm = this;
        vm.uid = $stateParams.uid;
        vm.date = $stateParams.date;
        vm.year = vm.date.split("-")[0];
        vm.month = vm.date.split("-")[1];
        vm.overtime = [];
        vm.employeeList = [];
        vm.createList = [];
        vm.editList = [];
        vm.createData = {};
        vm.editData = {};
        vm.addItem = addItem;
        vm.removeItem = removeItem;
        vm.editOvertime = editOvertime;

        getEmployees();
        getOvertime();

        function getEmployees(){
            debugger;
            OvertimeService.getEmployees(vm.uid).then(function(response){
                for(var i=0; i<response.employe_informations.length; i++){
                    vm.employeeList.push(response.employe_informations[i]);
                }
            });
        }

        function getOvertime(){
            debugger;
            OvertimeService.getOvertime(vm.uid).then(function(response){
                for(var i=0; i<response.length; i++){
                    for(var j=0; j<response[i].overtime.length;j++){
                        if((response[i].overtime[j].overtime_date.split("-")[0] +""+response[i].overtime[j].overtime_date.split("-")[1]) == (vm.date.split("-")[0] + "" +vm.date.split("-")[1])){
                             vm.overtime.push({
                                 name: response[i].name,
                                 id: response[i].overtime[j].employe_id,
                                 overtime_id: response[i].overtime[j].id,
                                 date: response[i].overtime[j].overtime_date.split("-")[2]*1,
                                 start_time: response[i].overtime[j].start_time * 1,
                                 end_time: response[i].overtime[j].end_time * 1,
                                 remark: response[i].overtime[j].remark
                             })
                        }
                    }
                }
            });
        }

        function addItem(){
            debugger;
            vm.overtime.push({
                name: "",
                id: "",
                overtime_id:"-",
                date:"",
                start_time: "",
                end_time: ""
            });
        }

        function removeItem(){
            var newDataList=[];
            vm.selectedAll = false;
            angular.forEach(vm.overtime, function(selected){
                if(!selected.selected){
                    newDataList.push(selected);
                }
            });
            vm.overtime = newDataList;
            vm.index = 0;
        }

        function editOvertime(){
            debugger;
            for(var i=0; i<vm.overtime.length;i++){
                if(vm.overtime[i].overtime_id != "-"){
                    vm.editList.push({
                        id:vm.overtime[i].overtime_id,
                        overtime_date: vm.date.split("-")[0]+ "-" +vm.date.split("-")[1] + "-"+ vm.overtime[i].date,
                        start_time:moment(vm.overtime[i].start_time).format("HH:mm"),
                        end_time:moment(vm.overtime[i].end_time).format("HH:mm"),
                        remark: vm.overtime[i].remark
                    });
                }
                else{
                    vm.createList.push({
                        employe_id:vm.overtime[i].id,
                        overtime_date: vm.date.split("-")[0]+ "-" +vm.date.split("-")[1] + "-"+ vm.overtime[i].date,
                        start_time:moment(vm.overtime[i].start_time).format("HH:mm"),
                        end_time:moment(vm.overtime[i].start_time).format("HH:mm"),
                        remark: vm.overtime[i].remark

                    })
                }
            }

            vm.editData = {overtime:  vm.editList};
            vm.createData = {overtime: vm.createList};

            OvertimeService.addOvertime(vm.createData).then(function(response){
            });
            OvertimeService.editOvertime(vm.editData).then(function(response){
            });
            $state.go('main.overtimeUndeclared');

        }

    }
})();