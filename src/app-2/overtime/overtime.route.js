/**
 * Created by Yonas on 3/13/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.overtime')
        .config(moduleConfig);
    /*@ngNoInject*/
    function moduleConfig($stateProvider) {
        $stateProvider
            .state('main.overtimeAdd',{
                url: "/overtimeAdd",
                templateUrl: "app/overtime/overtime.add.html",
                controller: 'OvertimeAddCtrl',
                controllerAs: 'vm'
            })
            .state('main.overtimeHistory',{
                url: "/overtimeHistory",
                templateUrl: "app/overtime/overtime.history.html",
                controller: 'OvertimeHistoryCtrl',
                controllerAs: 'vm'
            })
            .state('main.overtimeDetail',{
                url: "/overtimeDetail/{uid}/{date}",
                templateUrl: "app/overtime/overtime.detail.html",
                controller: 'OvertimeDetailCtrl',
                controllerAs: 'vm'
            })
            .state('main.overtimeEdit',{
                url: "/overtimeEdit/{uid}/{date}",
                templateUrl: "app/overtime/overtime.edit.html",
                controller: 'OvertimeEditCtrl',
                controllerAs: 'vm'
            })
            .state('main.overtimeUndeclared',{
                url: "/overtimeUndeclared",
                templateUrl: "app/overtime/overtime.undeclared.html",
                controller: 'OvertimeUndeclaredCtrl',
                controllerAs: 'vm'
            })

    }

})();
