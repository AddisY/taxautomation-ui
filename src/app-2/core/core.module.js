/**
 * Created by acer1 on 10/25/2016.
 */
(function () {
    'use strict';

    angular.module('app.core', ['ui.router', 'ui.bootstrap', 'restangular', 'textAngular',
        'switcher','highcharts-ng','angularUtils.directives.dirPagination'])

})();
