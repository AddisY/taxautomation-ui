/**
 * Created by Yonas on 2/20/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.VatDeclaration')
        .config(moduleConfig);
    /*@ngNoInject*/
    function moduleConfig($stateProvider) {
        $stateProvider
            .state('main.WithholdDeclaration', {
                url: "/WithholdDeclaration",
                templateUrl: "app/WithholdDeclaration/WithholdDeclaration.html",
                controller: 'WithholdDeclarationCtrl',
                controllerAs: 'vm'
            })
    }

})();
