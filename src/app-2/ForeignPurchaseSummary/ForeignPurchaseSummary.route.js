/**
 * Created by Yonas on 2/19/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.ForeignPurchaseSummary')
        .config(moduleConfig);
    /*@ngNoInject*/
    function moduleConfig($stateProvider) {
        $stateProvider
            .state('main.ForeignPurchaseSummaryHistory', {
                url: "/ForeignPurchaseSummaryHistory",
                templateUrl: "app/ForeignPurchaseSummary/ForeignPurchaseSummaryHistory.html",
                controller: 'ForeignPurchaseSummaryHistoryCtrl',
                controllerAs: 'vm'
            })
            .state('main.ForeignPurchaseSummaryDetail',{
                url: "/ForeignPurchaseSummaryDetail/{uid}/{date}",
                templateUrl: "app/ForeignPurchaseSummary/ForeignPurchaseSummaryDetail.html",
                controller: 'ForeignPurchaseSummaryDetailCtrl',
                controllerAs: 'vm'
            })
            .state('main.ForeignPurchaseSummaryUndeclared',{
                url: "/ForeignPurchaseSummaryUndeclared",
                templateUrl: "app/ForeignPurchaseSummary/ForeignPurchaseSummaryUndeclared.html",
                controller: 'ForeignPurchaseSummaryUndeclaredCtrl',
                controllerAs: 'vm'
            })
            .state('main.ForeignPurchaseSummaryEdit',{
                url: "/ForeignPurchaseSummaryEdit/{uid}/{date}",
                templateUrl: "app/ForeignPurchaseSummary/ForeignPurchaseSummaryEdit.html",
                controller: 'ForeignPurchaseSummaryEditCtrl',
                controllerAs: 'vm'
            })
            .state('main.ForeignPurchaseSummaryAdd',{
                url: "/ForeignPurchaseSummaryAdd",
                templateUrl: "app/ForeignPurchaseSummary/ForeignPurchaseSummaryAdd.html",
                controller: 'ForeignPurchaseSummaryAddCtrl',
                controllerAs: 'vm'
            })
    }

})();
