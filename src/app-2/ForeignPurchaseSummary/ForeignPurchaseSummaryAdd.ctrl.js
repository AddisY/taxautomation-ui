/**
 * Created by Yonas on 2/19/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.ForeignPurchaseSummary')
        .controller('ForeignPurchaseSummaryAddCtrl',ForeignPurchaseSummaryAddCtrl);

    function ForeignPurchaseSummaryAddCtrl($scope, $stateParams, $state, $rootScope,ForeignPurchaseSummaryService){
        var vm = this;
        vm.companyId = "1";
        vm.purchasedItems = [];
        vm.addItem = addItem;
        vm.removeItem = removeItem;
        vm.purchaseTotal = purchaseTotal;
        vm.purchaseSummary = purchaseSummary;
        vm.index = 0;
        vm.purchaseTotal = purchaseTotal;

        function addItem(){
            debugger;
            vm.purchasedItems.push({
                index: vm.index+=1,
                dec_date: "",
                dec_number: "",
                bill_code: "",
                tax_price: "",
                item_name: ""
            });
        }

        function removeItem(){
            var newDataList=[];
            vm.selectedAll = false;
            angular.forEach(vm.purchasedItems, function(selected){
                if(!selected.selected){
                    newDataList.push(selected);
                }
            });
            vm.purchasedItems = newDataList;
            vm.index = 0;
        }

        function purchaseTotal(){
            vm.itemTotal = 0;
            vm.subTotal = 0;
            vm.vatTotal = 0;
            for(var i=0;i<vm.purchasedItems.length;i++){
                vm.itemTotal += (vm.purchasedItems[i].item_price);
                vm.subTotal += (vm.purchasedItems[i].tax_price * 1);
                vm.vatTotal += (vm.purchasedItems[i].item_price * 0.15);
            }
            debugger;

        }

        function purchaseSummary(){
            debugger;
            vm.itemsList = [];

            for(var i=0; i<vm.purchasedItems.length; i++){
                vm.itemsList.push({
                    company_id: vm.companyId,
                    dec_number: vm.year + "-" + vm.month + "-" +vm.purchasedItems[i].dec_number,
                    tax_price: vm.purchasedItems[i].tax_price,
                    dec_date: vm.year + "-" + vm.month + "-" + vm.purchasedItems[i].dec_date,
                    item_price: vm.purchasedItems[i].item_price,
                    bill_code: vm.purchasedItems[i].bill_code,
                    item_name: vm.purchasedItems[i].item_name
                });
            }
            vm.itemsData = {foreignpurchase:  vm.itemsList};

            ForeignPurchaseSummaryService.createPurchaseSummary(vm.itemsData).then(function(response){
                $state.go('main.ForeignPurchaseSummaryHistory');
            });
        };
    }
})();
