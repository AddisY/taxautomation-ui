/**
 * Created by Yonas on 2/19/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.ForeignPurchaseSummary')
        .controller('ForeignPurchaseSummaryDetailCtrl',ForeignPurchaseSummaryDetailCtrl);

    function ForeignPurchaseSummaryDetailCtrl($scope, $stateParams, $rootScope,ForeignPurchaseSummaryService){
        var vm = this;
        vm.date = $stateParams.date;
        vm.uid = $stateParams.uid;
        vm.year = vm.date.split("-")[0];
        vm.month = vm.date.split("-")[1];

        getSummaryDetail();

        function getSummaryDetail(){
            debugger;
            vm.purchaseSummary = [];
            vm.purchaseTotal = 0;
            vm.purchaseVAT = 0;
            ForeignPurchaseSummaryService.getSummaryDetail(vm.date,vm.uid).then(function(response){
                for(var i=0; i<response.data.length; i++){
                    if(response.data[i].status == '1'){
                        vm.purchaseSummary.push(response.data[i]);
                    }
                }
                for(var i=0;vm.purchaseSummary.length;i++){
                    vm.purchaseTotal += vm.purchaseSummary[i].tax_price;
                    vm.purchaseVAT += vm.purchaseSummary[i].VAT;
                }
            });
        }
    }
})();
