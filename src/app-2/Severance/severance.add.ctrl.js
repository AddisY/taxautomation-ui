/**
 * Created by Yonas on 3/17/2017.
 */
(function () {
    'use strict';
    angular
        .module('app.severance')
        .controller('SeveranceAddCtrl', SeveranceAddCtrl);
    function SeveranceAddCtrl($scope, $state, $rootScope, SeveranceService) {
        var vm = this;
        vm.uid = '1';
        vm.employeeList = [];
        vm.severance = [];
        vm.severanceList = [];
        getEmployees();
        vm.addItem = addItem;
        vm.removeItem = removeItem;
        vm.addSeverance = addSeverance;

        function getEmployees() {
            debugger;
            SeveranceService.getEmployees(vm.uid).then(function (response) {
                for (var i = 0; i < response.employe_informations.length; i++) {
                    vm.employeeList.push(response.employe_informations[i]);
                }
            });
        }

        function addItem() {
            debugger;
            vm.severance.push({
                id: "",
                date: "",
                type: ""
            });
        }

        function removeItem() {
            var newDataList = [];
            vm.selectedAll = false;
            angular.forEach(vm.severance, function (selected) {
                if (!selected.selected) {
                    newDataList.push(selected);
                }
            });
            vm.severance = newDataList;
            vm.index = 0;
        }

        function addSeverance() {
            debugger;
            for (var i = 0; i < vm.severance.length; i++) {
                vm.severanceList.push({
                    employe_id: vm.severance[i].id,
                    end_date: vm.year + "-" + vm.month + "-" + vm.severance[i].date,
                    type: vm.severance[i].type
                });
            }
             vm.severanceData = {termination:vm.severanceList};
            SeveranceService.createSeverance(vm.severanceData).then(function(response){
               $state.go('main.SeveranceHistory');
            });
        }


    }
})();