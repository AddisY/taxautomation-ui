/**
 * Created by Yonas on 3/17/2017.
 */
(function () {
    'use strict';
    angular
        .module('app.severance')
        .controller('SeveranceEditCtrl', SeveranceEditCtrl);
    function SeveranceEditCtrl($stateParams, $state, $rootScope, SeveranceService) {
        var vm = this;
        vm.uid = $stateParams.uid;
        vm.date = $stateParams.date;
        vm.year = vm.date.split("-")[0];
        vm.month = vm.date.split("-")[1];
        vm.severance = [];
        vm.employeeList = [];
        vm.editItems = [];
        vm.createItems = [];
        vm.editData = [];
        vm.createData = [];
        getEmployees();
        getSeverance();
        vm.addItem = addItem;
        vm.removeItem = removeItem;
        vm.editSeverance = editSeverance;

        function getEmployees() {
            debugger;
            SeveranceService.getEmployees(vm.uid).then(function (response) {
                for (var i = 0; i < response.employe_informations.length; i++) {
                    vm.employeeList.push(response.employe_informations[i]);
                }
            });
        }

        function getSeverance() {
            SeveranceService.getSeverance(vm.uid).then(function (response) {
                for (var i = 0; i < response.length; i++) {
                    if ((response[i].termination.end_date.split("-")[0] + "" + response[i].termination.end_date.split("-")[1]) == (vm.date.split("-")[0] + "" + vm.date.split("-")[1])) {
                        vm.severance.push({
                            severanceID: response[i].termination.id,
                            id: response[i].termination.employe_id,
                            end_date: response[i].termination.end_date.split("-")[2] * 1,
                            type: response[i].termination.type
                        })
                    }
                }
            });
        }

        function addItem() {
            vm.severance.push({
                severanceID: "-",
                id: "",
                end_date: "",
                type: ""
            })
        }

        function removeItem() {
            var newDataList = [];
            vm.selectedAll = false;
            angular.forEach(vm.severance, function (selected) {
                if (!selected.selected) {
                    newDataList.push(selected);
                }
            });
            vm.severance = newDataList;
            vm.index = 0;
        }

        function editSeverance() {
            for (var i = 0; i < vm.severance.length; i++) {
                if (vm.severance[i].severanceID != "-") {
                    vm.editItems.push({
                            id: vm.severance[i].severanceID,
                            employe_id: vm.severance[i].id,
                            end_date: vm.year + "-" + vm.month + "-" + vm.severance[i].end_date,
                            type: vm.severance[i].type
                    })
                }
                else {
                    vm.createItems.push({
                            employe_id: vm.severance[i].id,
                            end_date: vm.year + "-" + vm.month + "-" + vm.severance[i].end_date,
                            type: vm.severance[i].type
                    })
                }
            }
            vm.editData = {termination:vm.editItems};
            vm.createData = {termination:vm.createItems};
            debugger;
            SeveranceService.editSeverance(vm.editData).then(function(response){
            });
            SeveranceService.createSeverance(vm.createData).then(function(response){
                $state.go('main.SeveranceUndeclared');
            });
        }
    }
})();