/**
 * Created by Yonas on 2/18/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.TaxablePurchaseSummary')
        .service('TaxablePurchaseSummaryService', TaxablePurchaseSummaryService);
    /*@ngNoInject*/
    function TaxablePurchaseSummaryService(TokenRestangular, $rootScope) {

        var service = {
            getSummaryHistory: getSummaryHistory,
            getSummaryDetail: getSummaryDetail,
            createPurchaseSummary: createPurchaseSummary,
            editPurchaseSummary: editPurchaseSummary
        };
        return service;

        function getSummaryHistory(){
            debugger;
            return TokenRestangular.all('localpurchase').customGET('');
        }

        function getSummaryDetail(date,uid){
            debugger;
            return TokenRestangular.all('localpurchase/'+date+'/'+uid).customGET('');
        }

        function createPurchaseSummary(data){
            debugger;
            return TokenRestangular.all('localpurchase').customPOST(data);
        }
        function editPurchaseSummary(data){
            debugger;
            return TokenRestangular.all('localpurchase').customPUT(data);
        }
    }

})();