/**
 * Created by Yonas on 2/18/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.TaxablePurchaseSummary')
        .controller('TaxablePurchaseSummaryHistoryCtrl',TaxablePurchaseSummaryHistoryCtrl);

    function TaxablePurchaseSummaryHistoryCtrl($scope, $state, $rootScope,TaxablePurchaseSummaryService){
        var vm = this;
        vm.companyID = "1";

        getSummaryHistory();

        function getSummaryHistory(){
            vm.companySummary = [];
            vm.purchaseSummary = [];
            vm.summary = [];
            var flags = [];
            TaxablePurchaseSummaryService.getSummaryHistory().then(function(response){
                for(var i=0; i<response.data.length; i++){
                    if(response.data[i].company_id == vm.companyID && response.data[i].status == '1' && response.data[i].purchase_type == 'product'){
                            vm.companySummary.push(response.data[i]);
                    }
                }

                for(var i=0; i< vm.companySummary.length; i++){
                    vm.summary[i] = vm.companySummary[i].bill_date.split("-")[0] + "/" + vm.companySummary[i].bill_date.split("-")[1];
                    if( flags[vm.summary[i]]) continue;
                    flags[vm.summary[i]] = true;
                    vm.purchaseSummary.push(vm.companySummary[i]);
                }

            });
        }

    }
})();
