(function () {
    'use strict';

    angular
        .module('app.auth')
        .config(moduleConfig);
    /*@ngNoInject*/
    function moduleConfig($stateProvider) {

        $stateProvider
            .state('login', {
                url: "/login",
                templateUrl: "app/auth/login.html",
                controller: 'AuthCtrl',
                controllerAs: 'vm'
            })
            .state('register', {
                url: "/register",
                templateUrl: "app/auth/register.html",
                controller: 'AuthCtrl',
                controllerAs: 'vm'
            })

    }

})();
