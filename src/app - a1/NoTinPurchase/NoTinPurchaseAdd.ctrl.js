/**
 * Created by Yonas on 2/20/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.NoTinPurchase')
        .controller('NoTinPurchaseAddCtrl',NoTinPurchaseAddCtrl);

    function NoTinPurchaseAddCtrl($scope, $stateParams, $state, $rootScope,NoTinPurchaseService){
        var vm = this;
        vm.companyId = "1";
        vm.purchasedItems = [];
        vm.addItem = addItem;
        vm.removeItem = removeItem;
        vm.purchaseTotal = purchaseTotal;
        vm.purchaseSummary = purchaseSummary;
        vm.index = 0;

        function addItem(){
            debugger;
            vm.purchasedItems.push({
                index: vm.index+=1,
                company_name:"",
                item_price:"",
                company_id:"",
                zone:"",
                sub_city:"",
                purchase_type:"",
                woreda:"",
                house_number:"",
                voucher_number:"",
                voucher_date:""
            });
        };

        function removeItem(){
            var newDataList=[];
            vm.selectedAll = false;
            angular.forEach(vm.purchasedItems, function(selected){
                if(!selected.selected){
                    newDataList.push(selected);
                }
            });
            vm.purchasedItems = newDataList;
            vm.index = 0;
        };

        function purchaseTotal(){
            vm.subTotal = 0;
            vm.withholdTotal = 0;
            for(var i=0;i<vm.purchasedItems.length;i++){
                vm.subTotal += (vm.purchasedItems[i].item_price * 1);
                vm.withholdTotal += (vm.purchasedItems[i].item_price * 0.02);
            }
        }

        function purchaseSummary(){
            debugger;
            vm.itemsList = [];

            for(var i=0; i<vm.purchasedItems.length; i++){
                vm.itemsList.push({
                    company_name: vm.purchasedItems[i].company_name,
                    item_price: vm.purchasedItems[i].item_price,
                    company_id:vm.companyId,
                    zone: vm.purchasedItems[i].zone,
                    sub_city: vm.purchasedItems[i].sub_city,
                    purchase_type: vm.purchasedItems[i].purchase_type,
                    woreda: vm.purchasedItems[i].woreda,
                    house_number: vm.purchasedItems[i].house_number,
                    voucher_number: vm.purchasedItems[i].voucher_number,
                    voucher_date: vm.year + "-" + vm.month + "-"+ vm.purchasedItems[i].voucher_date
                });
            }
            vm.itemsData = {nonregpurchase:  vm.itemsList};

            NoTinPurchaseService.createPurchaseSummary(vm.itemsData).then(function(response){
                $state.go('main.NoTinPurchaseHistory');
            });
        };
    }
})();