/**
 * Created by Yonas on 2/20/2017.
 */
(function (){
    'use strict';
    angular
        .module('app.NoTinPurchase')
        .controller('NoTinPurchaseDetailCtrl',NoTinPurchaseDetailCtrl);

    function NoTinPurchaseDetailCtrl($state,$stateParams,NoTinPurchaseService){
         var vm = this;
        vm.date = $stateParams.date;
        vm.uid = $stateParams.uid;
         getSummaryDetail();

        function getSummaryDetail(){
          debugger;
            vm.purchaseSummary = [];
            vm.purchaseTotal = 0;
          NoTinPurchaseService.getSummaryDetail(vm.date,vm.uid).then(function(response){

              for(var i=0; i<response.data.length; i++){
                  vm.purchaseSummary.push(response.data[i]);
                  vm.purchaseTotal += vm.purchaseSummary[i].item_price;
              }
          });
        }
    }
})();