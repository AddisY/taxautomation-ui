(function () {
    'use strict';

    angular
        .module('app.core')
        .config(moduleConfig);
    /*@ngNoInject*/
    function moduleConfig($stateProvider, $urlRouterProvider, RestangularProvider) {

        $urlRouterProvider.otherwise("/login");

      /*  RestangularProvider.setErrorInterceptor(
            function (response) {
                if (response.status == 401) {
                    window.location.href = 'index.html#/login';
                }
            }
        );*/
        $stateProvider
            .state('main', {
                url: "/main",
                templateUrl: "app/core/core.html",
                controller: 'CoreCtrl',
                controllerAs: 'vm'
            });

    }

})();
