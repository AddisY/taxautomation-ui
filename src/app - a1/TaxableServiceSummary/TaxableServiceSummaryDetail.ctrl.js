/**
 * Created by Yonas on 2/19/2017.
 */

(function(){
    'use strict';
    angular
        .module('app.TaxableServiceSummary')
        .controller('TaxableServiceSummaryDetailCtrl',TaxableServiceSummaryDetailCtrl);

    function TaxableServiceSummaryDetailCtrl($scope, $stateParams, $rootScope,TaxableServiceSummaryService){
        var vm = this;
        vm.date = $stateParams.date;
        vm.uid = $stateParams.uid;
        vm.year = vm.date.split("-")[0];
        vm.month = vm.date.split("-")[1];

        getSummaryDetail();

        function getSummaryDetail(){
            debugger;
            vm.serviceSummary = [];
            vm.serviceTotal = 0;
            vm.serviceVAT = 0;
            vm.withholdTotal = 0;
            TaxableServiceSummaryService.getSummaryDetail(vm.date,vm.uid).then(function(response){
                for(var i=0; i<response.data.length; i++){
                    if(response.data[i].purchase_type == 'service' && response.data[i].status == '1'){
                        vm.serviceSummary.push(response.data[i]);
                    }
                }

                for(var j=0; j<vm.serviceSummary.length; j++){
                    vm.serviceTotal += vm.serviceSummary[j].item_price;
                    vm.serviceVAT += vm.serviceSummary[j].VAT;
                    vm.withholdTotal += vm.serviceSummary[j].withholding_tax;
                }
            });
        }
    }
})();