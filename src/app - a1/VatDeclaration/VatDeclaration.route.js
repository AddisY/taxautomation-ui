/**
 * Created by Yonas on 2/20/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.VatDeclaration')
        .config(moduleConfig);
    /*@ngNoInject*/
    function moduleConfig($stateProvider) {
        $stateProvider
            .state('main.VatDeclaration', {
                url: "/VatDeclaration",
                templateUrl: "app/VatDeclaration/VatDeclaration.html",
                controller: 'VatDeclarationCtrl',
                controllerAs: 'vm'
            })
    }

})();
