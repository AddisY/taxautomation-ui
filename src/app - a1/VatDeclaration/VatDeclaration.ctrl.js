/**
 * Created by Yonas on 2/20/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.VatDeclaration')
        .controller('VatDeclarationCtrl',VatDeclarationCtrl);

    function VatDeclarationCtrl($scope, $stateParams, $rootScope,VatDeclarationService){
        var vm = this;
        vm.uid = '1';
        vm.date = '';
        vm.val1 = 0;
        vm.val2 = 0;
        vm.val3 = 0;
        vm.net = 0;
        vm.return = 0;
        vm.data ={};

        vm.vatReport = vatReport;
        vm.calculate = calculate;
        vm.vatDeclaration = vatDeclaration;

        function vatReport(){
            debugger;
            vm.date = vm.year + '-' + vm.month + '-1';
            VatDeclarationService.getVatReport(vm.date,vm.uid).then(function(response){
                vm.data = response.data;
                if(vm.data.salessummery_vat >= (vm.data.total_input_value * 0.15)){
                    vm.net = vm.data.salessummery_vat - (vm.data.total_input_value * 0.15);
                    vm.return = 0;
                }
                else if(vm.data.salessummery_vat < (vm.data.total_input_value * 0.15))
                {
                    vm.net = 0;
                    vm.return = (vm.data.total_input_value * 0.15) - vm.data.salessummery_vat;
                }
            });
            calculate();
        }

        function calculate(){
            debugger;
            vm.totalSales = 0;
            vm.totalSales = vm.data.salessummery + vm.val1 + vm.val2 + vm.val3;
        }

        function vatDeclaration(){
            debugger;
            VatDeclarationService.vatDeclaration(vm.date,vm.uid).then(function(response){
                alert("Vat Has been declared!");
            });

        }
    }
})();