/**
 * Created by Yonas on 3/13/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.payroll')
        .config(moduleConfig);
    /*@ngNoInject*/
    function moduleConfig($stateProvider) {
        $stateProvider
            .state('main.payrollDeclaration',{
                url: "/payrollDeclaration",
                templateUrl: "app/PayrollDeclaration/payroll.declaration.html",
                controller: 'payrollDeclarationCtrl',
                controllerAs: 'vm'
            })
    }

})();
