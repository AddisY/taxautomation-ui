/**
 * Created by Yonas on 3/13/2017.
 */
(function () {
    'use strict';
    angular
        .module('app.payroll')
        .controller('payrollDeclarationCtrl', payrollDeclarationCtrl);
    function payrollDeclarationCtrl(PayrollService) {
        var vm = this;
        vm.uid = "1";
        vm.company = {};
        vm.totalSalary = "";
        vm.total1 = 0;
        vm.total2 = 0;
        vm.total3 = 0;
        vm.total4 = 0;
        vm.employees = [];
        vm.terminated = [];
        vm.getPayroll = getPayroll;

        function getPayroll() {
            debugger;
            var date = vm.year + "-" + vm.month + "-01";
            PayrollService.getPayroll(vm.uid, date).then(function (response) {
                vm.company = response.company_Info;
                vm.totalSalary = response.total_salary;
                vm.employeeCount = response.numberof_employe;
                for (var i = 0; i < response.employe_info.length; i++) {
                    vm.employees.push(response.employe_info[i]);
                    vm.total1+=vm.employees[i].taxable_income;
                    vm.total2+=vm.employees[i].income_tax;
                    vm.total3+=vm.employees[i].cost_sharing;
                    vm.total4+=vm.employees[i].net_payment;
                }
                for (var j = 0; j < response.terminated.length; j++) {
                    vm.terminated.push(response.terminated[j]);
                }
            });
        }

    }
})();