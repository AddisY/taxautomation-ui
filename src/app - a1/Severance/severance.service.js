/**
 * Created by Yonas on 3/17/2017.
 */
(function (){
    'use strict';
    angular
        .module('app.severance')
        .service('SeveranceService',SeveranceService);

    function SeveranceService(TokenRestangular, $rootScope){
           var service = {
               getEmployees: getEmployees,
               createSeverance: createSeverance,
               getSeverance: getSeverance,
               getSeveranceDetail: getSeveranceDetail,
               editSeverance: editSeverance
           };
           return service;

        function getEmployees(companyID){
            debugger;
            return TokenRestangular.all('employebycompany/'+companyID).customGET('');
        }

        function createSeverance(data){
            debugger;
            return TokenRestangular.all('termination').customPOST(data);
        }

        function getSeverance(companyID){
            debugger;
            return TokenRestangular.all('termination/'+companyID).customGET('');
        }

        function getSeveranceDetail(uid,date){
            debugger;
            return TokenRestangular.all('serverancepay/'+uid+"/"+date).customGET('');
        }

        function editSeverance(data){
            debugger;
            return TokenRestangular.all('termination').customPUT(data);
        }

    }
})();