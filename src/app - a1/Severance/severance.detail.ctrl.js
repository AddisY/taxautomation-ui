/**
 * Created by Yonas on 3/17/2017.
 */
(function () {
    'use strict';
    angular
        .module('app.severance')
        .controller('SeveranceDetailCtrl', SeveranceDetailCtrl);
    function SeveranceDetailCtrl($scope, $stateParams, $rootScope, SeveranceService) {
        var vm = this;
        vm.date = $stateParams.date;
        vm.uid = $stateParams.uid;
        vm.severance = [];
        getSeveranceDetail();

        function getSeveranceDetail() {
            debugger;
            SeveranceService.getSeveranceDetail(vm.uid, vm.date).then(function (response) {
                for (var i = 0; i < response.length; i++) {
                    vm.severance.push(response[i]);
                }
            });
        }
    }
})();