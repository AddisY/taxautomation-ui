/**
 * Created by Yonas on 3/17/2017.
 */
(function (){
   'use strict';
   angular
      .module('app.severance')
      .controller('SeveranceUndeclaredCtrl',SeveranceUndeclaredCtrl);

    function SeveranceUndeclaredCtrl($scope, $state, $rootScope,SeveranceService){
        var vm = this;
        vm.uid = '1';
        vm.severance = [];
        var flags = [];
        vm.date = [];
        getSeverance();

        function getSeverance() {
            debugger;
            SeveranceService.getSeverance(vm.uid).then(function (response) {
                for (var i = 0; i < response.length; i++) {
                    if (response[i].termination.status == '0') {
                        vm.date[i] = response[i].termination.end_date.split("-")[0] + "" + response[i].termination.end_date.split("-")[1];
                        if (flags[vm.date[i]]) continue;
                        flags[vm.date[i]] = true;
                        vm.severance.push(response[i].termination);
                    }
                }
            });
        }
    }   
})();