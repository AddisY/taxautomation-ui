/**
 * Created by Yonas on 3/17/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.severance')
        .config(moduleConfig);
    /*@ngNoInject*/
    function moduleConfig($stateProvider) {
        $stateProvider
            .state('main.SeveranceHistory', {
                url: "/SeveranceHistory",
                templateUrl: "app/Severance/severance.history.html",
                controller: 'SeveranceHistoryCtrl',
                controllerAs: 'vm'
            })
            .state('main.SeveranceDetail', {
                url: "/SeveranceDetail/{uid}/{date}",
                templateUrl: "app/Severance/severance.detail.html",
                controller: 'SeveranceDetailCtrl',
                controllerAs: 'vm'
            })
            .state('main.SeveranceUndeclared', {
                url: "/SeveranceUndeclared",
                templateUrl: "app/Severance/severance.undeclared.html",
                controller: 'SeveranceUndeclaredCtrl',
                controllerAs: 'vm'
            })
            .state('main.SeveranceEdit', {
                url: "/SeveranceEdit/{uid}/{date}",
                templateUrl: "app/Severance/severance.edit.html",
                controller: 'SeveranceEditCtrl',
                controllerAs: 'vm'
            })
            .state('main.SeveranceAdd', {
                url: "/SeveranceAdd",
                templateUrl: "app/Severance/severance.add.html",
                controller: 'SeveranceAddCtrl',
                controllerAs: 'vm'
            })
    }

})();
