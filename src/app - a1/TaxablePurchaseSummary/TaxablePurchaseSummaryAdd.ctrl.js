/**
 * Created by Yonas on 2/18/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.TaxablePurchaseSummary')
        .controller('TaxablePurchaseSummaryAddCtrl',TaxablePurchaseSummaryAddCtrl);

    function TaxablePurchaseSummaryAddCtrl($scope, $state,$stateParams, $rootScope,TaxablePurchaseSummaryService){
        var vm = this;
        vm.companyID = "1";
        vm.index = 0;
        vm.purchasedItems = [];
        vm.addItem = addItem;
        vm.removeItem = removeItem;
        vm.purchaseSummary = purchaseSummary;
        vm.purchaseTotal = purchaseTotal;


        function addItem(){
            debugger;
            vm.purchasedItems.push({
                item_name: "",
                item_price: "",
                company_name: "",
                tin_number: "",
                mrc_code: "",
                company_id: "",
                bill_code: "",
                bill_date: ""
            });
        }

        function removeItem(){
            var newDataList=[];
            vm.selectedAll = false;
            angular.forEach(vm.purchasedItems, function(selected){
                if(!selected.selected){
                    newDataList.push(selected);
                }
            });
            vm.purchasedItems = newDataList;
            vm.index = 0;
        }

        function purchaseTotal(){
            vm.subTotal = 0;
            vm.vatTotal = 0;
            vm.withholdTotal = 0;
            for(var i=0;i<vm.purchasedItems.length;i++){
                vm.subTotal += (vm.purchasedItems[i].item_price);
                vm.vatTotal += (vm.purchasedItems[i].item_price * 0.15);
                vm.withholdTotal += (vm.purchasedItems[i].item_price * 0.02);
            }
            debugger;

        }

        function purchaseSummary(){
            debugger;
            vm.itemsList = [];

            for(var i=0; i<vm.purchasedItems.length; i++){
                vm.itemsList.push({
                    company_name: vm.purchasedItems[i].company_name,
                    tin_number: vm.purchasedItems[i].tin_number,
                    mrc_code: vm.purchasedItems[i].mrc_code,
                    item_name: vm.purchasedItems[i].item_name,
                    item_price: vm.purchasedItems[i].item_price,
                    company_id: vm.companyID,
                    bill_code: vm.purchasedItems[i].bill_code,
                    bill_date:vm.year + "-" + vm.month + "-" + vm.purchasedItems[i].bill_date,
                    withholding_date: vm.year + "-" + vm.month + "-" + vm.purchasedItems[i].withholding_date,
                    withholding_code: vm.purchasedItems[i].withholding_code,
                    item_type: "local;",
                    withholding_tax_number : "12121",
                    purchase_type: "product"
                });
            }
            vm.itemsData = {locapurchase:  vm.itemsList};

            TaxablePurchaseSummaryService.createPurchaseSummary(vm.itemsData).then(function(response){
                $state.go('main.TaxablePurchaseSummaryHistory');
            });
        }

    }
})();