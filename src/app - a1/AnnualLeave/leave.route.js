/**
 * Created by Yonas on 3/15/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.leave')
        .config(moduleConfig);
    /*@ngNoInject*/
    function moduleConfig($stateProvider) {
        $stateProvider
            .state('main.LeaveRecord', {
                url: "/LeaveRecord",
                templateUrl: "app/AnnualLeave/leave.history.html",
                controller: 'LeaveRecordCtrl',
                controllerAs: 'vm'
            })
            .state('main.LeaveDetail', {
                url: "/LeaveDetail/{uid}/{date}",
                templateUrl: "app/AnnualLeave/leave.detail.html",
                controller: 'LeaveDetailCtrl',
                controllerAs: 'vm'
            })
            .state('main.LeaveUndeclared', {
                url: "/LeaveUndeclared",
                templateUrl: "app/AnnualLeave/leave.undeclared.html",
                controller: 'LeaveUndeclaredCtrl',
                controllerAs: 'vm'
            })
            .state('main.LeaveEdit', {
                url: "/LeaveEdit/{uid}/{date}",
                templateUrl: "app/AnnualLeave/leave.edit.html",
                controller: 'LeaveEditCtrl',
                controllerAs: 'vm'
            })
            .state('main.LeaveAdd', {
                url: "/LeaveAdd",
                templateUrl: "app/AnnualLeave/leave.add.html",
                controller: 'LeaveAddCtrl',
                controllerAs: 'vm'
            })
    }

})();
