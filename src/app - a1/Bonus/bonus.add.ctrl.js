/**
 * Created by Yonas on 3/15/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.bonus')
        .controller('BonusAddCtrl',BonusAddCtrl);

    function BonusAddCtrl($state,BonusService){
        var vm = this;
        vm.uid = "1";
        vm.employeeList = [];
        vm.employee = [];
        vm.bonus = [];
        vm.bonusData = {};
        getEmployees();
        vm.addItem = addItem;
        vm.removeItem = removeItem;
        vm.addBonus = addBonus;


        function getEmployees(){
            BonusService.getEmployees(vm.uid).then(function(response){
               for(var i=0; i<response.employe_informations.length;i++){
                  vm.employeeList.push(response.employe_informations[i]);
               }
            });
        }

        function addItem(){
          vm.employee.push({
               employeeId: "",
               leaveDate: "",
               payday: "",
               total_amount: ""
          });
        }

        function removeItem(){
            var newDataList=[];
            vm.selectedAll = false;
            angular.forEach(vm.employee, function(selected){
                if(!selected.selected){
                    newDataList.push(selected);
                }
            });
            vm.employee = newDataList;
            vm.index = 0;
        }

       
        function addBonus(){
            for(var i=0;i<vm.employee.length;i++){
                vm.bonus.push({
                   employe_id: vm.employee[i].id,
                   pay_day: vm.year+"-"+vm.month+"-"+vm.employee[i].date,
                   served_leaved_period: vm.employee[i].month+"-"+vm.employee[i].days,
                   total_amount:vm.employee[i].bonusAmount
                });
            }
            vm.bonusData = {bonus:vm.bonus};
            BonusService.addBonus(vm.bonusData).then(function(response){
               $state.go('main.BonusHistory') ;
            });
        }
    
    }
})();