/**
 * Created by Yonas on 3/20/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.allowance')
        .config(moduleConfig);
    /*@ngNoInject*/
    function moduleConfig($stateProvider) {
        $stateProvider
            .state('main.AllowanceHistory', {
                url: "/AllowanceHistory",
                templateUrl: "app/Allowance/allowance.history.html",
                controller: 'AllowanceHistoryCtrl',
                controllerAs: 'vm'
            })
            .state('main.AllowanceDetail', {
                url: "/AllowanceDetail/{uid}/{date}",
                templateUrl: "app/Allowance/allowance.detail.html",
                controller: 'AllowanceDetailCtrl',
                controllerAs: 'vm'
            })
            .state('main.AllowanceUndeclared', {
                url: "/AllowanceUndeclared",
                templateUrl: "app/Allowance/allowance.undeclared.html",
                controller: 'AllowanceUndeclaredCtrl',
                controllerAs: 'vm'
            })
            .state('main.AllowanceEdit', {
                url: "/AllowanceEdit/{uid}/{date}",
                templateUrl: "app/Allowance/allowance.edit.html",
                controller: 'AllowanceEditCtrl',
                controllerAs: 'vm'
            })
            .state('main.AllowanceAdd', {
                url: "/AllowanceAdd",
                templateUrl: "app/Allowance/allowance.add.html",
                controller: 'AllowanceAddCtrl',
                controllerAs: 'vm'
            })
    }

})();
