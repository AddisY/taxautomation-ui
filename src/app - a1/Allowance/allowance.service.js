/**
 * Created by Yonas on 3/20/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.allowance')
        .service('AllowanceService', AllowanceService);
    function AllowanceService(TokenRestangular){

        var service = {
            getAllowanceHistory: getAllowanceHistory,
            getAllowanceDetail: getAllowanceDetail,
            getAllAllowance: getAllAllowance,
            getEmployeeList: getEmployeeList,
            addAllowance: addAllowance,
            editAllowance: editAllowance
        };

        return service;

        function getAllowanceHistory(companyID){
            debugger;
            return TokenRestangular.all('allowancebycompany/'+companyID).customGET('');
        }

        function getAllAllowance(companyID){
            debugger;
            return TokenRestangular.all('allowanceall/'+companyID).customGET('');
        }

        function getAllowanceDetail(uid,date){
            debugger;
            return TokenRestangular.all('allowancebydate/'+uid+"/"+date).customGET('');
        }

        function getEmployeeList(companyID){
            debugger;
            return TokenRestangular.all('employebycompany/'+companyID).customGET('');
        }

        function addAllowance(data){
            debugger;
            return TokenRestangular.all('allowance').customPOST(data);
        }

        function editAllowance(data){
            debugger;
            return TokenRestangular.all('allowance').customPUT(data);
        }
    }
})();