/**
 * Created by Yonas on 3/13/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.overtime')
        .controller('OvertimeAddCtrl',OvertimeAddCtrl);

    function OvertimeAddCtrl($scope, $state, $rootScope,OvertimeService){
        var vm = this;
        vm.uid = '1';
        vm.employeeList = [];
        vm.employee = [];
        vm.overtimeList = [];
        vm.overtimeData = {};
        getEmployees();
        vm.addItem = addItem;
        vm.removeItem = removeItem;
        vm.addOvertime = addOvertime;

        function getEmployees(){
            debugger;
            OvertimeService.getEmployees(vm.uid).then(function(response){
                for(var i=0; i<response.employe_informations.length; i++){
                    vm.employeeList.push(response.employe_informations[i]);
                }
            });
        }

        function addItem(){
            debugger;
            vm.employee.push({
                id: "",
                date: "",
                startTime: "",
                endTime: "",
                remark: ""
            });
        }

        function removeItem(){
            var newDataList=[];
            vm.selectedAll = false;
            angular.forEach(vm.employee, function(selected){
                if(!selected.selected){
                    newDataList.push(selected);
                }
            });
            vm.employee = newDataList;
            vm.index = 0;
        }

        function addOvertime(){
            debugger;
            for(var i=0; i<vm.employee.length; i++){
               vm.overtimeList.push({
                   employe_id: vm.employee[i].id,
                   overtime_date: vm.year + '-' + vm.month + '-' + vm.employee[i].date,
                   start_time: moment(vm.employee[i].startTime).format("HH:mm"),
                   end_time: moment(vm.employee[i].endTime).format("HH:mm"),
                   remark: vm.employee[i].remark
               });
            }

            vm.overtimeData = {overtime: vm.overtimeList};

            OvertimeService.addOvertime(vm.overtimeData).then(function(response){
                $state.go('main.overtimeUndeclared') ;
            });

        }


    }
})();
