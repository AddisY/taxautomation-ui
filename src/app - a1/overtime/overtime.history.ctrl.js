/**
 * Created by Yonas on 3/14/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.overtime')
        .controller('OvertimeHistoryCtrl',OvertimeHistoryCtrl);

    function OvertimeHistoryCtrl($scope, $state, $rootScope,OvertimeService){
        var vm = this;
        vm.uid = '1';
        vm.overtime = [];
        var flags = [];
        vm.date = [];
        getOvertime();

        function getOvertime(){
            debugger;
            OvertimeService.getOvertime(vm.uid).then(function(response){
                for(var i=0; i<response.length;i++){
                    debugger;
                    for(var j=0; j<response[i].overtime.length;j++){
                        if(response[i].overtime[j].status == '1'){
                            vm.date[j] = response[i].overtime[j].overtime_date.split("-")[0] + "" +response[i].overtime[j].overtime_date.split("-")[1];
                            if( flags[vm.date[j]]) continue;
                            flags[vm.date[j]] = true;
                            vm.overtime.push(response[i].overtime[j]);
                        }
                    }
                }
            });
        }

    }
})();
