/**
 * Created by Yonas on 2/19/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.ForeignPurchaseSummary')
        .controller('ForeignPurchaseSummaryHistoryCtrl',ForeignPurchaseSummaryHistoryCtrl);

    function ForeignPurchaseSummaryHistoryCtrl($scope, $state, $rootScope,ForeignPurchaseSummaryService){
        var vm = this;
        vm.companyID = "1";
        var flags = [];
        getSummaryHistory();

        function getSummaryHistory(){
            vm.companySummary = [];
            vm.purchaseSummary = [];
            vm.summary = [];
            ForeignPurchaseSummaryService.getSummaryHistory().then(function(response){
                for(var i=0; i<response.data.length; i++){
                    if(response.data[i].company_id == vm.companyID && response.data[i].status == '1'){
                        vm.companySummary.push(response.data[i]);
                    }
                }
                for(var i=0; i< vm.companySummary.length; i++){
                    vm.summary[i] = vm.companySummary[i].dec_date.split("-")[0] + "/" + vm.companySummary[i].dec_date.split("-")[1];
                    if( flags[vm.summary[i]]) continue;
                    flags[vm.summary[i]] = true;
                    vm.purchaseSummary.push(vm.companySummary[i]);
                }

            });
        }

    }
})();
