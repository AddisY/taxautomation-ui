/**
 * Created by Yonas on 2/19/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.ForeignPurchaseSummary')
        .controller('ForeignPurchaseSummaryEditCtrl',ForeignPurchaseSummaryEditCtrl);

    function ForeignPurchaseSummaryEditCtrl($scope, $stateParams, $state, $rootScope,ForeignPurchaseSummaryService){
        var vm = this;
        vm.date = $stateParams.date;
        vm.uid = $stateParams.uid;
        vm.purchasedItems = [];
        vm.addItem = addItem;
        vm.removeItem = removeItem;
        vm.purchaseTotal = purchaseTotal;
        vm.purchaseSummary = purchaseSummary;
        vm.year = vm.date.split("-")[0];
        vm.month = vm.date.split("-")[1];

        getSummaryDetail();

        function getSummaryDetail(){
            debugger;
            ForeignPurchaseSummaryService.getSummaryDetail(vm.date,vm.uid).then(function(response){
                for(var i=0; i<response.data.length; i++){
                    if(response.data[i].status == '0'){
                        vm.purchasedItems.push({
                            id: response.data[i].id,
                            dec_date: response.data[i].dec_date.split("-")[2]*1,
                            dec_number: response.data[i].dec_number,
                            bill_code: response.data[i].bill_code,
                            tax_price: response.data[i].tax_price,
                            item_name: response.data[i].item_name,
                            item_price: response.data[i].item_price
                        });
                    }
                }
            });
        }

        function addItem(){
            debugger;
            vm.purchasedItems.push({
                id: "-",
                dec_date: "",
                dec_number: "",
                bill_code: "",
                tax_price: "",
                item_name: ""
            });
        }

        function removeItem(){
            var newDataList=[];
            vm.selectedAll = false;
            angular.forEach(vm.purchasedItems, function(selected){
                if(!selected.selected){
                    newDataList.push(selected);
                }
            });
            vm.purchasedItems = newDataList;
            vm.index = 0;
        }

        function purchaseTotal(){
            vm.itemTotal = 0;
            vm.subTotal = 0;
            vm.vatTotal = 0;
            for(var i=0;i<vm.purchasedItems.length;i++){
                vm.itemTotal += (vm.purchasedItems[i].item_price * 1);
                vm.subTotal += (vm.purchasedItems[i].tax_price * 1);
                vm.vatTotal += (vm.purchasedItems[i].item_price * 0.15);
            }
            debugger;

        }

        function purchaseSummary(){
            debugger;
            vm.itemsList = [];
            vm.editList = [];
            vm.createList = [];

            for(var i=0; i<vm.purchasedItems.length; i++){
                if(vm.purchasedItems[i].id != "-"){
                    vm.editList.push({
                        id: vm.purchasedItems[i].id,
                        dec_number: vm.purchasedItems[i].dec_number,
                        tax_price: vm.purchasedItems[i].tax_price,
                        dec_date: vm.year +"-"+vm.month+"-"+vm.purchasedItems[i].dec_date,
                        withholding_date: vm.year +"-"+vm.month+"-"+vm.purchasedItems[i].dec_date,
                        bill_code: vm.purchasedItems[i].bill_code,
                        item_name: vm.purchasedItems[i].item_name,
                        item_price: vm.purchasedItems[i].item_price
                    });
                }
                else{
                    vm.createList.push({
                        company_id: vm.uid,
                        dec_number: vm.purchasedItems[i].dec_number,
                        tax_price: vm.purchasedItems[i].tax_price,
                        dec_date: vm.year + "-" + vm.month + "-" + vm.purchasedItems[i].dec_date,
                        withholding_date: vm.year + "-" + vm.month + "-" + vm.purchasedItems[i].dec_date,
                        bill_code: vm.purchasedItems[i].bill_code,
                        item_name: vm.purchasedItems[i].item_name,
                        item_price: vm.purchasedItems[i].item_price
                    });
                }
            }
            vm.editData = {foreignpurchase:  vm.editList};
            vm.createData = {foreignpurchase: vm.createList};

            ForeignPurchaseSummaryService.createPurchaseSummary(vm.createData).then(function(response){
            });
            ForeignPurchaseSummaryService.editPurchaseSummary(vm.editData).then(function(response){
            });
            $state.go('main.ForeignPurchaseSummaryUndeclared');
        }
    }
})();
