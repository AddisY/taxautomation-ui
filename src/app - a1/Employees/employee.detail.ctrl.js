/**
 * Created by Yonas on 3/10/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.employees')
        .controller('EmployeeDetailCtrl',EmployeeDetailCtrl);

    function EmployeeDetailCtrl($scope, $stateParams, $rootScope,EmployeesService){
        var vm = this;
        var employeeID = $stateParams.employeeID;
        vm.employeeDetail = {};
        vm.salaryType = "";
        vm.employeeOvertime = [];
        vm.salaryType = "";
        vm.termination = {};
        vm.salary = [];
        vm.overtime = [];
        vm.allowance = [];
        vm.bonus = [];
        vm.leave = [];

        getEmployeeDetail();

        function getEmployeeDetail(){
            debugger;
            EmployeesService.getEmployeeDetail(employeeID).then(function(response){
                vm.salaryType = response.employe_informations[0].salary[0].payment_type;
                vm.employeeDetail = response.employe_informations[0];
                vm.termination = response.employe_informations[0].termination;
                for(var i=0; i<vm.employeeDetail.overtime.length; i++){
                    vm.overtime.push(vm.employeeDetail.overtime[i]);
                }
                for(var i=0; i<vm.employeeDetail.salary.length; i++){
                    vm.salary.push(vm.employeeDetail.salary[i]);
                }
                for(var i=0; i<vm.employeeDetail.allowance.length; i++){
                    vm.allowance.push(vm.employeeDetail.allowance[i]);
                }
                for(var i=0; i<vm.employeeDetail.bonus.length; i++){
                    vm.bonus.push(vm.employeeDetail.bonus[i]);
                }
                for(var i=0; i<vm.employeeDetail.leave.length; i++){
                    vm.leave.push(vm.employeeDetail.leave[i]);
                }
            });
        }
    }

})();