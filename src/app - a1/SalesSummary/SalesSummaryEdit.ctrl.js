/**
 * Created by Yonas on 12/24/2016.
 */

(function(){
    'use strict';
    angular
        .module('app.SalesSummary')
        .controller('SalesSummaryEditCtrl',SalesSummaryEditCtrl);

    function SalesSummaryEditCtrl($scope, $state,$stateParams, $rootScope,SalesSummaryService){
        var vm = this;
        vm.date = $stateParams.date;
        vm.companyID = $stateParams.uid;
        vm.index = 0;
        vm.salesItems = [];
        getSalesItems();
        salesTotal();
        vm.addItem = addItem;
        vm.removeItem = removeItem;
        vm.salesSummary = salesSummary;
        vm.salesTotal = salesTotal;
        vm.year = vm.date.split("-")[0];
        vm.month = vm.date.split("-")[1];
        vm.isSaving = false;
        vm.subTotal = 0;
        vm.vatTotal = 0;
    

        function getSalesItems(){
          debugger;
            SalesSummaryService.getSummaryDetail(vm.date,vm.companyID).then(function(response){
                for(var i=0; i<response.data.length; i++){
                    vm.salesItems.push({
                        name: response.data[i].item_name,
                        price: response.data[i].item_price,
                        itemID: response.data[i].id,
                        companyID: response.data[i].company_id,
                        code: response.data[i].bill_code,
                        date: response.data[i].bill_date.split("-")[2] * 1
                    });
                }
            });
        }

        function addItem(){
            debugger;
            vm.salesItems.push({
                index: vm.index+=1,
                itemID: "-",
                name: "",
                price: "",
                companyID: "",
                code: "",
                date: ""
            });
        }

        function removeItem(){
            var newDataList=[];
            vm.selectedAll = false;
            angular.forEach(vm.salesItems, function(selected){
                if(!selected.selected){
                    newDataList.push(selected);
                }
            });
            vm.salesItems = newDataList;
            vm.index = 0;
        }

        function salesTotal(){
            for(var i=0;i<vm.salesItems.length;i++){
                vm.subTotal += vm.salesItems[i].price * 1;
                vm.vatTotal += vm.salesItems[i].price * 0.15;
            }
            debugger;

        }

        function salesSummary(){
            debugger;
            vm.isSaving = true;
            vm.itemsList = [];
            vm.editList = [];
            vm.createList = [];
            for(var i=0; i<vm.salesItems.length; i++){
                if(vm.salesItems[i].itemID != "-"){
                    vm.editList.push({
                        id: vm.salesItems[i].itemID,
                        item_name: vm.salesItems[i].name,
                        item_price: vm.salesItems[i].price,
                        bill_code: vm.salesItems[i].code,
                        bill_date:vm.year +"-" + vm.month +"-"+vm.salesItems[i].date
                    })
                }
                else{
                    vm.createList.push({
                        item_name: vm.salesItems[i].name,
                        item_price: vm.salesItems[i].price,
                        company_id: vm.companyID,
                        bill_code: vm.salesItems[i].code,
                        bill_date:vm.year + "-" + vm.month + "-" + vm.salesItems[i].date
                    });
                }
            }

            vm.editData = {localsales:  vm.editList};
            vm.createData = {localsales: vm.createList};

            SalesSummaryService.editSalesSummary(vm.editData).then(function(response){
                SalesSummaryService.createSalesSummary(vm.createData).then(function(response){
                    vm.isSaving = false;
                });

            });

            $state.go('main.SalesSummaryHistory');
        }

    }
})();
