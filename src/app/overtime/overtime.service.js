/**
 * Created by Yonas on 3/13/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.overtime')
        .service('OvertimeService', OvertimeService);
    /*@ngNoInject*/
    function OvertimeService(TokenRestangular, $rootScope) {

        var service = {
            getEmployees: getEmployees,
            getEmployee: getEmployee,
            addOvertime: addOvertime,
            getOvertime: getOvertime,
            getOvertimeDetail: getOvertimeDetail,
            editOvertime: editOvertime
        };

        return service;

        function getEmployees(companyID){
            debugger;
            return TokenRestangular.all('employebycompany/'+companyID).customGET('');
        }

        function getEmployee(employeeID){
            debugger;
            return TokenRestangular.all('employeinfo/'+employeeID).customGET('');
        }

        function addOvertime(data){
            debugger;
            return TokenRestangular.all('overtime').customPOST(data);
        }

        function getOvertime(companyID){
            debugger;
            return TokenRestangular.all('overtime/'+companyID).customGET('');
        }

        function getOvertimeDetail(uid,date){
            debugger;
            return TokenRestangular.all('overtimebycompany/'+uid + '/'+date).customGET('');
        }

        function editOvertime(data){
            debugger;
            return TokenRestangular.all('overtime').customPUT(data);
        }
    }

})();

