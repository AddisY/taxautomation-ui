/**
 * Created by Yonas on 2/20/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.WithholdDeclaration')
        .controller('WithholdDeclarationCtrl',WithholdDeclarationCtrl);

    function WithholdDeclarationCtrl($scope, $stateParams, $rootScope,WithholdDeclarationService){
        var vm = this;
        vm.uid = '1';
        vm.date = '';
        vm.withholdReport = withholdReport;
        vm.totalNumberWithholdees = '';
        vm.totalTaxableAount = '';
        vm.totalWithholdTax = '';

        function withholdReport(){
            debugger;
            vm.data = [];
            vm.date = vm.year + '-' + vm.month + '-1';
            WithholdDeclarationService.getWithholdReport(vm.date,vm.uid).then(function(response){
                for(var i=0; i<response.withholding.length; i++){
                     vm.data.push(response.withholding[i]);
                }
                vm.totalNumberWithholdees = response.totalNumberWithholdees;
                vm.totalTaxableAount = response.totalTaxableAount;
                vm.totalWithholdTax = response.totalWithholdTax;
            });
        }

    }
})();