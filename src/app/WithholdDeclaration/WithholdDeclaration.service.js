/**
 * Created by Yonas on 2/20/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.WithholdDeclaration')
        .service('WithholdDeclarationService', WithholdDeclarationService);
    /*@ngNoInject*/
    function WithholdDeclarationService(TokenRestangular, $rootScope) {
    
        var service = {
            getWithholdReport: getWithholdReport
        };
        return service;

        function getWithholdReport(data,uid){
            debugger;
            return TokenRestangular.all('withhold_report/'+data+'/'+uid).customGET('');
        }

    }

})();