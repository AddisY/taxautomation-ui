/**
 * Created by Yonas on 3/25/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.leave')
        .controller('LeaveEditCtrl',LeaveEditCtrl);

    function LeaveEditCtrl($stateParams,$state,LeaveService){
        var vm = this;
        vm.uid= $stateParams.uid;
        vm.date = $stateParams.date;
        vm.year = vm.date.split("-")[0];
        vm.month = vm.date.split("-")[1];
        vm.annual_leave = '';
        vm.working_days = '';
        vm.leave = [];
        vm.employeeList = [];
        vm.editItems = [];
        vm.createItems = [];
        vm.editLeave = {};
        vm.addLeave = {};
        getEmployee();
        getLeaveDetail();
        vm.addItem = addItem;
        vm.removeItem = removeItem;
        vm.editLeave = editLeave;

        function getEmployee(){
            debugger;
            LeaveService.getEmployeeList(vm.uid).then(function(response){
                for(var i=0; i<response.employe_informations.length; i++){
                    vm.employeeList.push(response.employe_informations[i]);
                }
            });
        }

        function getLeaveDetail(){
            debugger;
            LeaveService.getLeaveRecord(vm.uid).then(function(response){
                for(var i=0;i<response.length;i++){
                    for(var j=0;j<response[i].leave.length;j++){
                        if((response[i].leave[j].issued_date.split("-")[0]+""+response[i].leave[j].issued_date.split("-")[1]) == (vm.date.split("-")[0]+""+vm.date.split("-")[1]) && response[i].leave[j].status == '0'){
                            vm.leave.push({
                                leaveID: response[i].leave[j].id,
                                annual_leave: response[i].leave[j].annual_leave,
                                working_date: response[i].leave[j].working_date,
                                id: response[i].leave[j].employe_id,
                                issuedDate: response[i].leave[j].issued_date.split("-")[2] * 1,
                                month: response[i].leave[j].served_leaved_period.split("-")[0] * 1,
                                days: response[i].leave[j].served_leaved_period.split("-")[1] * 1,
                                accumulated: response[i].leave[j].leave_accumulated * 1
                            })
                        }
                    }
                }
                vm.annual_leave = vm.leave[0].annual_leave;
                vm.working_days = vm.leave[0].working_date;
            });

        }

        function addItem(){
            vm.leave.push({
                leaveID: "-",
                id: "",
                issuedDate: "",
                month: "",
                days: "",
                accumulated: ""
            });
        }

        function removeItem(){
            var newDataList=[];
            vm.selectedAll = false;
            angular.forEach(vm.leave, function(selected){
                if(!selected.selected){
                    newDataList.push(selected);
                }
            });
            vm.leave = newDataList;
            vm.index = 0;
        }

        function editLeave(){
            debugger;
            for(var i=0;i<vm.leave.length;i++){
                if(vm.leave[i].leaveID != "-"){
                    vm.editItems.push({
                        id: vm.leave[i].leaveID,
                        leave_date: vm.annual_leave,
                        working_date: vm.working_days,
                        annual_leave: vm.leave[i].accumulated,
                        issued_date: vm.year + "-" +vm.month + "-" +vm.leave[i].issuedDate,
                        served_leave_period: vm.leave[i].month + "-" + vm.leave[i].days,
                        leave_accumulated: vm.leave[i].accumulated
                    });
                }
                else{
                    vm.createItems.push({
                        employe_id: vm.leave[i].id,
                        leave_date: vm.annual_leave,
                        working_date: vm.working_days,
                        annual_leave: vm.leave[i].accumulated,
                        issued_date: vm.year + "-" +vm.month + "-" +vm.leave[i].issuedDate,
                        served_leave_period: vm.leave[i].month + "-" + vm.leave[i].days,
                        leave_accumulated: vm.leave[i].accumulated
                    })
                }
            }

            vm.editLeave = {leave:vm.editItems};
            vm.addLeave = {leave:vm.createItems};

            debugger;
            LeaveService.editLeave(vm.editLeave).then(function(response){
            });
            LeaveService.createLeave(vm.addLeave).then(function(response){
                $state.go('main.LeaveUndeclared');
            });

        }



    }
})();