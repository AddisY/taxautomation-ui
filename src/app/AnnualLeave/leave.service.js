/**
 * Created by Yonas on 3/15/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.core')
        .service('LeaveService', LeaveService);
    /*@ngNoInject*/
    function LeaveService(TokenRestangular, appConstant, $rootScope) {
        var service = {
            getEmployeeList: getEmployeeList,
            getLeaveRecord: getLeaveRecord,
            getFixedLeave: getFixedLeave,
            getVariableLeave: getVariableLeave,
            createLeave: createLeave,
            editLeave: editLeave
        };
        return service;

        function getEmployeeList(companyID){
            debugger;
            return TokenRestangular.all('employebycompany/'+companyID).customGET('');
        }

        function createLeave(data){
            debugger;
            return TokenRestangular.all('leave').customPOST(data);
        }

        function getLeaveRecord(companyID){
            debugger;
            return TokenRestangular.all('leave/'+companyID).customGET('');
        }

        function getFixedLeave(uid,date){
            debugger;
            return TokenRestangular.all('fixedleavebycompany/'+uid+'/'+date).customGET('');
        }

        function getVariableLeave(uid,date){
            debugger;
            return TokenRestangular.all('variableleavebycompany/'+uid+'/'+date).customGET('');
        }

        function editLeave(data){
            debugger;
            return TokenRestangular.all('leave').customPUT(data);
        }

    }

})();
