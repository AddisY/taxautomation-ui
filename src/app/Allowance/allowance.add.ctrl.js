/**
 * Created by Yonas on 3/20/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.allowance')
        .controller('AllowanceAddCtrl',AllowanceAddCtrl);
    function AllowanceAddCtrl($scope, $state, $rootScope,AllowanceService){
        var vm = this;
        vm.companyID = "1";
        vm.employeeList = [];
        vm.allowance = [];
        vm.allowanceItems = [];
        vm.allowanceData = {};
        getEmployees();
        vm.addItem = addItem;
        vm.removeItem = removeItem;
        vm.addAllowance = addAllowance;

        function getEmployees(){
            debugger;
            AllowanceService.getEmployeeList(vm.companyID).then(function(response){
                for(var i=0; i<response.employe_informations.length; i++){
                    vm.employeeList.push(response.employe_informations[i]);
                }
            });
        }

        function addItem(){
            vm.allowance.push({
                id:"",
                type:"",
                startDate:"",
                endDate:"",
                amount:""
            });
        }

        function removeItem(){
            var newDataList=[];
            vm.selectedAll = false;
            angular.forEach(vm.allowance, function(selected){
                if(!selected.selected){
                    newDataList.push(selected);
                }
            });
            vm.allowance = newDataList;
            vm.index = 0;
        }

        function addAllowance(){
          debugger;
          for(var i=0; i<vm.allowance.length;i++){
             if(vm.allowance[i].description != undefined){
                 vm.allowanceItems.push({
                     employe_id: vm.allowance[i].id,
                     type: vm.allowance[i].type,
                     description: vm.allowance[i].description,
                     start: vm.year + "-" + vm.month + "-" +vm.allowance[i].startDate,
                     end: vm.year + "-" + vm.month + "-" + vm.allowance[i].endDate,
                     total_amount: vm.allowance[i].amount
                 });
             }
              else{
                 vm.allowanceItems.push({
                     employe_id: vm.allowance[i].id,
                     type: vm.allowance[i].type,
                     description: "",
                     start: vm.year + "-" + vm.month + "-" +vm.allowance[i].startDate,
                     end: vm.year + "-" + vm.month + "-" + vm.allowance[i].endDate,
                     total_amount: vm.allowance[i].amount
                 });
             }
          }
            vm.allowanceData = {allowance:vm.allowanceItems};

            AllowanceService.addAllowance(vm.allowanceData).then(function(){
               $state.go('main.AllowanceHistory');
            });
        }
    }
})();