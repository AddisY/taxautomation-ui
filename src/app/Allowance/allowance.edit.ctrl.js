/**
 * Created by Yonas on 3/20/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.allowance')
        .controller('AllowanceEditCtrl',AllowanceEditCtrl);
    function AllowanceEditCtrl($stateParams, $state, $rootScope,AllowanceService){
        var vm = this;
        vm.uid= $stateParams.uid;
        vm.date = $stateParams.date;
        vm.year = vm.date.split("-")[0];
        vm.month = vm.date.split("-")[1];
        vm.employeeList = [];
        vm.allowance = [];
        vm.editItems = [];
        vm.createItems = [];
        vm.editData = {};
        vm.addData = {};
        getEmployeeList();
        getAllowanceDetail();
        vm.addItem = addItem;
        vm.removeItem = removeItem;
        vm.editAllowance = editAllowance;

        function getEmployeeList(){
            AllowanceService.getEmployeeList(vm.uid).then(function(response){
                for(var i=0; i<response.employe_informations.length; i++){
                    vm.employeeList.push(response.employe_informations[i]);
                }
            });
        }

        function getAllowanceDetail(){
            AllowanceService.getAllAllowance(vm.uid).then(function(response){
                for(var i=0;i<response.length;i++){
                    for(var j=0; j<response[i].allowance.length;j++){
                        if((response[i].allowance[j].end.split("-")[0]+""+response[i].allowance[j].end.split("-")[1])==(vm.date.split("-")[0]+""+vm.date.split("-")[1])){
                           vm.allowance.push({
                               allowanceID: response[i].allowance[j].id,
                               id: response[i].allowance[j].employe_id,
                               type: response[i].allowance[j].type,
                               description: response[i].allowance[j].description,
                               start: response[i].allowance[j].start.split("-")[2] * 1,
                               end: response[i].allowance[j].end.split("-")[2] * 1,
                               amount:response[i].allowance[j].total_amount
                           })
                        }
                    }
                }
            });
        }

        function addItem(){
            vm.allowance.push({
                allowanceID:"-",
                id: "",
                type:"",
                startDate:"",
                endDate:"",
                amount:""
            })
        }

        function removeItem(){
            var newDataList=[];
            vm.selectedAll = false;
            angular.forEach(vm.allowance, function(selected){
                if(!selected.selected){
                    newDataList.push(selected);
                }
            });
            vm.allowance = newDataList;
            vm.index = 0;
        }

        function editAllowance(){
            for(var i=0;i<vm.allowance.length;i++){
                if(vm.allowance[i].allowanceID != "-" && vm.allowance[i].description != ""){
                    vm.editItems.push({
                        id: vm.allowance[i].allowanceID,
                        type: vm.allowance[i].type,
                        description: vm.allowance[i].description,
                        start: vm.year+"-"+vm.month+"-"+ vm.allowance[i].start,
                        end: vm.year+"-"+vm.month+"-"+ vm.allowance[i].end,
                        total_amount: vm.allowance[i].amount
                    })
                }
                else if(vm.allowance[i].allowanceID != "-" && vm.allowance[i].description == ""){
                    vm.editItems.push({
                        id: vm.allowance[i].allowanceID,
                        type: vm.allowance[i].type,
                        description: "",
                        start: vm.year+"-"+vm.month+"-"+ vm.allowance[i].start,
                        end: vm.year+"-"+vm.month+"-"+ vm.allowance[i].end,
                        total_amount: vm.allowance[i].amount
                    })
                }
                else if(vm.allowance[i].allowanceID == "-" && vm.allowance[i].description != ""){
                     vm.createItems.push({
                         employe_id: vm.allowance[i].id,
                         type: vm.allowance[i].type,
                         description: vm.allowance[i].description,
                         start: vm.year + "-" + vm.month + "-" +vm.allowance[i].start,
                         end: vm.year + "-" + vm.month + "-" + vm.allowance[i].end,
                         total_amount: vm.allowance[i].amount
                     })
                }
                else if(vm.allowance[i].allowanceID == "-" && vm.allowance[i].description == ""){
                    vm.createItems.push({
                        employe_id: vm.allowance[i].id,
                        type: vm.allowance[i].type,
                        description: "",
                        start: vm.year + "-" + vm.month + "-" +vm.allowance[i].start,
                        end: vm.year + "-" + vm.month + "-" + vm.allowance[i].end,
                        total_amount: vm.allowance[i].amount
                    })
                }
            }

            vm.editData = {allowance:vm.editItems};
            vm.createData = {allowance: vm.createItems};

            AllowanceService.editAllowance(vm.editData).then(function(response){
            });
            AllowanceService.addAllowance(vm.createData).then(function(response){
                $state.go('main.AllowanceUndeclared');
            });
            debugger;
        }
    }
})();