/**
 * Created by Yonas on 3/10/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.core')
        .service('EmployeesService', EmployeesService);
    /*@ngNoInject*/
    function EmployeesService(TokenRestangular, appConstant, $rootScope) {
        var service = {
            getEmployeeList: getEmployeeList,
            getEmployeeDetail: getEmployeeDetail,
            addEmployee: addEmployee,
            deleteEmployee: deleteEmployee,
            getEmployeeOvertime: getEmployeeOvertime
        };
        return service;

        function getEmployeeList(companyID){
            debugger;
            return TokenRestangular.all('employebycompany/'+companyID).customGET('');
        }

        function getEmployeeDetail(employeeID){
            debugger;
            return TokenRestangular.all('employeinfo/'+employeeID).customGET('');
        }

        function addEmployee(employeeData){
            debugger;
            return TokenRestangular.all('employeinfo').customPOST(employeeData);
        }

        function deleteEmployee(employeeID){
            debugger;
            return TokenRestangular.all('employeinfo/'+employeeID).customDELETE('');
        }

        function getEmployeeOvertime(employeeID){
            debugger;
            return TokenRestangular.all('overtimebyemp/'+employeeID).customGET('');
        }
    }

})();
