/**
 * Created by Yonas on 3/12/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.employees')
        .controller('EmployeeAddCtrl',EmployeeAddCtrl);

    function EmployeeAddCtrl($scope, $state, $rootScope,EmployeesService){
        var vm = this;
        vm.uid = '1';
        vm.employeeData = [];
        vm.employee = {};
        vm.salaryDuration = [];
        vm.salary = [];
        vm.addItem = addItem;
        vm.removeItem = removeItem;
        vm.addEmployee = addEmployee;
        vm.index = 0;

        function addItem(){
            debugger;
            vm.salaryDuration.push({
                salaryMonth: "",
                totalSalary: ""
            });
        };

        function removeItem(){
            var newDataList=[];
            vm.selectedAll = false;
            angular.forEach(vm.salaryDuration, function(selected){
                if(!selected.selected){
                    newDataList.push(selected);
                }
            });
            vm.salaryDuration = newDataList;
            vm.index = 0;
        };

        function addEmployee(){
            if(vm.salaryType == 'fixed'){
                vm.employeeData.push({
                    employe:{
                        full_name: vm.fname +" "+ vm.mname+ " " + vm.lname,
                        company_id: vm.uid,
                        tin_number: vm.tinNumber,
                        department: vm.department,
                        start_date: vm.year + '-' + vm.month + '-' + vm.day,
                        cost_sharing: vm.costSharing
                    },
                    salary: [{
                        total_salary: vm.salary,
                        payment_type: vm.salaryType,
                        duration: vm.fiscalYear,
                        pay_day: vm.year + '-' + vm.month + '-01'
                    }]
                });
            }
            else{
                for(var i=0; i<vm.salaryDuration.length; i++){
                    vm.salary.push({
                        total_salary: vm.salaryDuration[i].totalSalary,
                        payment_type: vm.salaryType,
                        duration: vm.fiscalYear + "-"+vm.salaryDuration[i].salaryMonth,
                        pay_day: vm.fiscalYear + '-' + vm.salaryDuration[i].salaryMonth + '-01'
                    });
                }
                vm.employeeData.push({
                    employe:{
                        full_name: vm.fname + vm.mname + vm.lname,
                        company_id: vm.uid,
                        tin_number: vm.tinNumber,
                        department: vm.department,
                        start_date: vm.year + '-' + vm.month + '-' + vm.day,
                        cost_sharing: vm.costSharing
                    },
                    salary: vm.salary
                });
            }


           vm.employee= {employeinfo: vm.employeeData};
           debugger;
           EmployeesService.addEmployee(vm.employee).then(function(response){
               $state.go('main.employeesList');
           });
       }
    }

})();