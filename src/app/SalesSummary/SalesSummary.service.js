/**
 * Created by Nejib on 12/24/2016.
 */
(function () {
    'use strict';

    angular
        .module('app.SalesSummary')
        .service('SalesSummaryService', SalesSummaryService);
    /*@ngNoInject*/
    function SalesSummaryService(TokenRestangular, $rootScope) {

        var service = {
            getSummaryHistory: getSummaryHistory,
            getSummaryDetail: getSummaryDetail,
            createSalesSummary: createSalesSummary,
            editSalesSummary: editSalesSummary,
        };
        return service;

        function getSummaryHistory(){
            debugger;
            return TokenRestangular.all('localsales').customGET('');
        }

        function getSummaryDetail(date,uid){
            debugger;
            return TokenRestangular.all('localsales/'+date+'/'+uid).customGET('');
        }

        function createSalesSummary(data){
            debugger;
            return TokenRestangular.all('localsales').customPOST(data);
        }
        function editSalesSummary(data){
            debugger;
            return TokenRestangular.all('localsales').customPUT(data);
        }
    }

})();

