/**
 * Created by Yonas on 2/18/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.TaxablePurchaseSummary')
        .config(moduleConfig);
    /*@ngNoInject*/
    function moduleConfig($stateProvider) {
        $stateProvider
            .state('main.TaxablePurchaseSummaryHistory', {
                url: "/TaxablePurchaseSummaryHistory",
                templateUrl: "app/TaxablePurchaseSummary/TaxablePurchaseSummaryHistory.html",
                controller: 'TaxablePurchaseSummaryHistoryCtrl',
                controllerAs: 'vm'
            })
            .state('main.TaxablePurchaseSummaryDetail',{
                url: "/TaxablePurchaseSummaryDetail/{uid}/{date}",
                templateUrl: "app/TaxablePurchaseSummary/TaxablePurchaseSummaryDetail.html",
                controller: 'TaxablePurchaseSummaryDetailCtrl',
                controllerAs: 'vm'
            })
            .state('main.TaxablePurchaseSummaryUndeclared',{
                url: "/TaxablePurchaseSummaryUndeclared",
                templateUrl: "app/TaxablePurchaseSummary/TaxablePurchaseSummaryUndeclared.html",
                controller: 'TaxablePurchaseSummaryUndeclaredCtrl',
                controllerAs: 'vm'
            })
            .state('main.TaxablePurchaseSummaryEdit',{
                url: "/TaxablePurchaseSummaryEdit/{uid}/{date}",
                templateUrl: "app/TaxablePurchaseSummary/TaxablePurchaseSummaryEdit.html",
                controller: 'TaxablePurchaseSummaryEditCtrl',
                controllerAs: 'vm'
            })
            .state('main.TaxablePurchaseSummaryAdd',{
                url: "/TaxablePurchaseSummaryAdd",
                templateUrl: "app/TaxablePurchaseSummary/TaxablePurchaseSummaryAdd.html",
                controller: 'TaxablePurchaseSummaryAddCtrl',
                controllerAs: 'vm'
            })
    }

})();
