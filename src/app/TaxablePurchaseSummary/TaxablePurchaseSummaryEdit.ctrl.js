/**
 * Created by Yonas on 2/18/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.TaxablePurchaseSummary')
        .controller('TaxablePurchaseSummaryEditCtrl',TaxablePurchaseSummaryEditCtrl);

    function TaxablePurchaseSummaryEditCtrl($scope, $state,$stateParams, $rootScope,TaxablePurchaseSummaryService){
        var vm = this;
        vm.date = $stateParams.date;
        vm.companyID = $stateParams.uid;
        vm.index = 0;
        vm.purchasedItems = [];
        getPurchasedItems();
        vm.addItem = addItem;
        vm.removeItem = removeItem;
        vm.purchaseSummary = purchaseSummary;
        vm.purchaseTotal = purchaseTotal;
        vm.year = vm.date.split("-")[0];
        vm.month = vm.date.split("-")[1];

        function getPurchasedItems(){
            debugger;
            TaxablePurchaseSummaryService.getSummaryDetail(vm.date,vm.companyID).then(function(response){
                for(var i=0; i<response.data.length; i++){
                    if(response.data[i].purchase_type == 'product') {
                        vm.purchasedItems.push({
                            id: response.data[i].id,
                            company_name: response.data[i].company_name,
                            tin_number: response.data[i].tin_number,
                            item_price: response.data[i].item_price,
                            mrc_code: response.data[i].mrc_code,
                            bill_code: response.data[i].bill_code,
                            bill_date: response.data[i].bill_date.split("-")[2] * 1,
                            withholding_code: response.data[i].withholding_code,
                            withholding_date: response.data[i].withholding_date.split("-")[2] * 1,
                            item_type: response.data[i].item_type,
                            purchase_type: response.data[i].purchase_type,
                            item_name: response.data[i].item_name
                        });
                    }
                }
            });
        }

        function addItem(){
            debugger;
            vm.purchasedItems.push({
                id: "-",
                item_name: "",
                item_price: "",
                company_name: "",
                tin_number: "",
                mrc_code: "",
                company_id: "",
                bill_code: "",
                bill_date: ""
            });
        }

        function removeItem(){
            var newDataList=[];
            vm.selectedAll = false;
            angular.forEach(vm.purchasedItems, function(selected){
                if(!selected.selected){
                    newDataList.push(selected);
                }
            });
            vm.purchasedItems = newDataList;
            vm.index = 0;
        }

        function purchaseTotal(){
            vm.subTotal = 0;
            vm.vatTotal = 0;
            vm.withholdTotal = 0;
            for(var i=0;i<vm.purchasedItems.length;i++){
                vm.subTotal += (vm.purchasedItems[i].item_price);
                vm.vatTotal += (vm.purchasedItems[i].item_price * 0.15);
                vm.withholdTotal += (vm.purchasedItems[i].item_price * 0.02);
            }
            debugger;

        }

        function purchaseSummary(){
            debugger;
            vm.itemsList = [];
            vm.itemsList = [];
            vm.editList = [];
            vm.createList = [];
            for(var i=0; i<vm.purchasedItems.length; i++){
                if(vm.purchasedItems[i].id != "-"){
                    vm.editList.push({
                        id: vm.purchasedItems[i].id,
                        company_name: vm.purchasedItems[i].company_name,
                        tin_number: vm.purchasedItems[i].tin_number,
                        mrc_code: vm.purchasedItems[i].mrc_code,
                        item_name: vm.purchasedItems[i].item_name,
                        item_price: vm.purchasedItems[i].item_price,
                        bill_code: vm.purchasedItems[i].bill_code,
                        bill_date:vm.year + "-" + vm.month + "-" + vm.purchasedItems[i].bill_date,
                        withholding_date:vm.year + "-" + vm.month + "-" +  vm.purchasedItems[i].withholding_date,
                        withholding_code: vm.purchasedItems[i].tin_number,
                        item_type: "local;",
                        withholding_tax_number : vm.purchasedItems[i].tin_number,
                        purchase_type: vm.purchasedItems[i].purchase_type
                    });
                }
                else
                    vm.createList.push({
                        company_name: vm.purchasedItems[i].company_name,
                        tin_number: vm.purchasedItems[i].tin_number,
                        mrc_code: vm.purchasedItems[i].mrc_code,
                        item_name: vm.purchasedItems[i].item_name,
                        item_price: vm.purchasedItems[i].item_price,
                        company_id: vm.companyID,
                        bill_code: vm.purchasedItems[i].bill_code,
                        bill_date:vm.year + "-" + vm.month + "-" + vm.purchasedItems[i].bill_date,
                        withholding_date: vm.year + "-" + vm.month + "-" + vm.purchasedItems[i].withholding_date,
                        withholding_code: vm.purchasedItems[i].withholding_code,
                        item_type: "local;",
                        withholding_tax_number : "12121",
                        purchase_type: "product"
                    });
            }

            vm.editData = {locapurchase:  vm.editList};
            vm.createData = {locapurchase: vm.createList};
            debugger;

            TaxablePurchaseSummaryService.editPurchaseSummary(vm.editData).then(function(response){
            });
            TaxablePurchaseSummaryService.createPurchaseSummary(vm.createData).then(function(response){
            });

            $state.go('main.TaxablePurchaseSummaryUndeclared');
        }

    }
})();