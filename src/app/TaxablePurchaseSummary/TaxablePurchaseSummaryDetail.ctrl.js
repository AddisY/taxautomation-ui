/**
 * Created by Yonas on 2/18/2017.
 */

(function(){
    'use strict';
    angular
        .module('app.TaxablePurchaseSummary')
        .controller('TaxablePurchaseSummaryDetailCtrl',TaxablePurchaseSummaryDetailCtrl);

    function TaxablePurchaseSummaryDetailCtrl($scope, $stateParams, $rootScope,TaxablePurchaseSummaryService){
        var vm = this;
        vm.date = $stateParams.date;
        vm.uid = $stateParams.uid;
        vm.year = '';
        vm.month = '';

        getSummaryDetail();

        function getSummaryDetail(){
            debugger;
            vm.purchaseSummary = [];
            vm.purchaseTotal = 0;
            vm.purchaseVAT = 0;
            vm.withholdTotal = 0;
            TaxablePurchaseSummaryService.getSummaryDetail(vm.date,vm.uid).then(function(response){
                for(var i=0; i<response.data.length; i++){
                    if(response.data[i].purchase_type == 'product' && response.data[i].status == '1'){
                        vm.purchaseSummary.push(response.data[i]);
                    }
                }
                vm.year = vm.purchaseSummary[0].bill_date.split("-")[0];
                vm.month = vm.purchaseSummary[0].bill_date.split("-")[1];

                for(var j=0; j<vm.purchaseSummary.length; j++){
                    vm.purchaseTotal += vm.purchaseSummary[j].item_price;
                    vm.purchaseVAT += vm.purchaseSummary[j].VAT;
                    vm.withholdTotal += vm.purchaseSummary[j].withholding_tax;
                }
            });
        }
    }
})();