/**
 * Created by acer1 on 10/26/2016.
 */
(function () {
    'use strict';

    angular
        .module('app.auth')
        .controller('AuthCtrl', AuthCtrl);
    /*@ngNoInject*/
    function AuthCtrl($scope, $state, $rootScope, TokenRestangular, AuthService, appConstant) {
        var vm = this;
        vm.loginError = '';
        vm.authenticating = false;
        vm.login = login;

        login();

        function login() {
            vm.authenticating = true;
            vm.loginError = '';
            debugger;
            var data = {
                "grant_type": appConstant.grant_type,
                "client_id": appConstant.client_id,
                "client_secret": appConstant.client_secret
            };
            $rootScope.uid = "1";

            debugger;
            AuthService.login(data).then(function (response) {
                debugger;
                localStorage.setItem('automation_token', response.access_token);
               /* localStorage.setItem('vas_refresh_token', response.refresh_token);*/
                TokenRestangular.setDefaultHeaders({Authorization: 'Bearer ' + localStorage.getItem('automation_token')});
                $state.go('main.SalesSummaryHistory');
            }, function (error) {
                debugger;
                if (error.statusText == 'Unauthorized') {
                    vm.loginError = "Invalid username or password !";
                }
                else {
                    vm.loginError = error.statusText ? error.statusText : 'Please Check Your Network Connection!';
                }
                vm.authenticating = false;
            })

        }
    }
})();
