/**
 * Created by Yonas on 3/13/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.overtime')
        .service('PensionService', PensionService);
    /*@ngNoInject*/
    function PensionService(TokenRestangular, $rootScope) {

        var service = {
            getPension: getPension
        };

        return service;

        function getPension(companyID,date){
            debugger;
            return TokenRestangular.all('employepension/'+companyID+'/'+date).customGET('');
        }
    }

})();
