/**
 * Created by Yonas on 2/19/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.NoTinPurchase')
        .service('NoTinPurchaseService', NoTinPurchaseService);
    /*@ngNoInject*/
    function NoTinPurchaseService(TokenRestangular, $rootScope) {

        var service = {
            getSummaryHistory: getSummaryHistory,
            getSummaryDetail: getSummaryDetail,
            createPurchaseSummary: createPurchaseSummary,
            editPurchaseSummary: editPurchaseSummary
        };
        return service;

        function getSummaryHistory(){
            debugger;
            return TokenRestangular.all('nonregpurchase').customGET('');
        }

        function getSummaryDetail(date,uid){
            debugger;
            return TokenRestangular.all('nonregpurchase/'+date+'/'+uid).customGET('');
        }

        function createPurchaseSummary(data){
            debugger;
            return TokenRestangular.all('nonregpurchase').customPOST(data);
        }

        function editPurchaseSummary(data){
            debugger;
            return TokenRestangular.all('nonregpurchase').customPUT(data);
        }
    }

})();

