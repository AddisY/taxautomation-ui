/**
 * Created by acer1 on 10/26/2016.
 */
(function () {
    'use strict';

    angular
        .module('app.core')
        .controller('CoreCtrl', CoreCtrl);
    /*@ngNoInject*/
    function CoreCtrl($scope, $state, $rootScope,CoreService,$http) {
        var vm = this;
        $rootScope.getLanguage = getLanguage;

        getLanguage("en-US");

        function getLanguage(lang) {
            
            // var lang = document.getElementsByTagName('html')[0].getAttribute('lang');
            //var lang = "en-US"; //am-ET
            $http.get('assets/lang/' + lang + '.json').then(function (response) {
                
                $rootScope.L = response.data;
            });

        }
    }
})();
