/**
 * Created by Yonas on 2/19/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.TaxableServiceSummary')
        .controller('TaxableServiceSummaryEditCtrl',TaxableServiceSummaryEditCtrl);

    function TaxableServiceSummaryEditCtrl($scope, $state,$stateParams, $rootScope,TaxableServiceSummaryService){
        var vm = this;
        vm.date = $stateParams.date;
        vm.companyID = $stateParams.uid;
        vm.serviceItems = [];
        getServiceItems();
        serviceTotal();
        vm.year = vm.date.split("-")[0];
        vm.month = vm.date.split("-")[1];
        vm.addItem = addItem;
        vm.removeItem = removeItem;
        vm.serviceSummary = serviceSummary;
        vm.serviceTotal = serviceTotal;

        function getServiceItems(){
            debugger;
            TaxableServiceSummaryService.getSummaryDetail(vm.date,vm.companyID).then(function(response){
                for(var i=0; i<response.data.length; i++){
                    if(response.data[i].purchase_type == 'service'){
                        vm.serviceItems.push({
                            id: response.data[i].id,
                            item_name: response.data[i].item_name,
                            item_price: response.data[i].item_price,
                            company_name: response.data[i].company_name,
                            tin_number: response.data[i].tin_number,
                            mrc_code: response.data[i].mrc_code,
                            company_id: response.data[i].company_id,
                            bill_code: response.data[i].bill_code,
                            bill_date: response.data[i].bill_date.split("-")[2] * 1,
                            withholding_code: response.data[i].withholding_code,
                            withholding_date: response.data[i].withholding_date.split("-")[2] * 1,
                            purchase_type: response.data[i].purchase_type
                        });
                    }
                }
            });
        }

        function addItem(){
            debugger;
            vm.serviceItems.push({
                id: "-",
                item_name: "",
                item_price: "",
                company_name: "",
                tin_number: "",
                mrc_code: "",
                company_id: "",
                bill_code: "",
                bill_date: ""
            });
        }

        function removeItem(){
            var newDataList=[];
            var i=0;
            vm.selectedAll = false;
            angular.forEach(vm.serviceItems, function(selected){
                if(!selected.selected){
                    newDataList.push(selected);
                }
            });
            vm.serviceItems = newDataList;
            vm.index = 0;
        }

        function serviceTotal(){
            vm.subTotal = 0;
            vm.vatTotal = 0;
            vm.withholdTotal = 0;
            for(var i=0;i<vm.serviceItems.length;i++){
                vm.subTotal += (vm.serviceItems[i].item_price);
                vm.vatTotal += (vm.serviceItems[i].item_price * 0.15);
                vm.withholdTotal += (vm.serviceItems[i].item_price * 0.02);
            }
            debugger;

        }

        function serviceSummary(){
            debugger;
            vm.itemsList = [];
            vm.editList = [];
            vm.createList = [];
            for(var i=0; i<vm.serviceItems.length; i++){
                if(vm.serviceItems[i].id != "-"){
                    vm.editList.push({
                        id: vm.serviceItems[i].id,
                        company_name: vm.serviceItems[i].company_name,
                        tin_number: vm.serviceItems[i].tin_number,
                        mrc_code: vm.serviceItems[i].mrc_code,
                        item_name: vm.serviceItems[i].item_name,
                        item_price: vm.serviceItems[i].item_price,
                        bill_code: vm.serviceItems[i].bill_code,
                        bill_date:vm.year + "-" + vm.month + "-" + vm.serviceItems[i].bill_date,
                        withholding_date:vm.year + "-" + vm.month + "-" +  vm.serviceItems[i].withholding_date,
                        withholding_code: vm.serviceItems[i].tin_number,
                        item_type: "local;",
                        withholding_tax_number : vm.serviceItems[i].tin_number,
                        purchase_type: vm.serviceItems[i].purchase_type
                    });
                }
                else
                    vm.createList.push({
                        company_name: vm.serviceItems[i].company_name,
                        tin_number: vm.serviceItems[i].tin_number,
                        mrc_code: vm.serviceItems[i].mrc_code,
                        item_name: vm.serviceItems[i].item_name,
                        item_price: vm.serviceItems[i].item_price,
                        company_id: vm.companyID,
                        bill_code: vm.serviceItems[i].bill_code,
                        bill_date:vm.year + "-" + vm.month + "-" + vm.serviceItems[i].bill_date,
                        withholding_date: vm.year + "-" + vm.month + "-" + vm.serviceItems[i].withholding_date,
                        withholding_code: vm.serviceItems[i].withholding_code,
                        item_type: "local;",
                        withholding_tax_number : "12121",
                        purchase_type: "product"
                    });
            }
            vm.editData = {locapurchase:  vm.editList};
            vm.createData = {locapurchase: vm.createList};
            debugger;

            TaxableServiceSummaryService.editServiceSummary(vm.editData).then(function(response){
            });
            TaxableServiceSummaryService.createServiceSummary(vm.createData).then(function(response){
            });

            $state.go('main.TaxableServiceSummaryUndeclared');
        };

    }
})();