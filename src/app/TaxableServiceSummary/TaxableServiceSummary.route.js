/**
 * Created by Yonas on 2/19/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.TaxableServiceSummary')
        .config(moduleConfig);
    /*@ngNoInject*/
    function moduleConfig($stateProvider) {
        $stateProvider
            .state('main.TaxableServiceSummaryHistory', {
                url: "/TaxableServiceSummaryHistory",
                templateUrl: "app/TaxableServiceSummary/TaxableServiceSummaryHistory.html",
                controller: 'TaxableServiceSummaryHistoryCtrl',
                controllerAs: 'vm'
            })
            .state('main.TaxableServiceSummaryDetail',{
                url: "/TaxableServiceSummaryDetail/{uid}/{date}",
                templateUrl: "app/TaxableServiceSummary/TaxableServiceSummaryDetail.html",
                controller: 'TaxableServiceSummaryDetailCtrl',
                controllerAs: 'vm'
            })
            .state('main.TaxableServiceSummaryUndeclared',{
                url: "/TaxableServiceSummaryUndeclared",
                templateUrl: "app/TaxableServiceSummary/TaxableServiceSummaryUndeclared.html",
                controller: 'TaxableServiceSummaryUndeclaredCtrl',
                controllerAs: 'vm'
            })
            .state('main.TaxableServiceSummaryEdit',{
                url: "/TaxableServiceSummaryEdit/{uid}/{date}",
                templateUrl: "app/TaxableServiceSummary/TaxableServiceSummaryEdit.html",
                controller: 'TaxableServiceSummaryEditCtrl',
                controllerAs: 'vm'
            })
            .state('main.TaxableServiceSummaryAdd',{
                url: "/TaxableServiceSummaryAdd",
                templateUrl: "app/TaxableServiceSummary/TaxableServiceSummaryAdd.html",
                controller: 'TaxableServiceSummaryAddCtrl',
                controllerAs: 'vm'
            })
    }

})();
