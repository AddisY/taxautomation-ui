/**
 * Created by Yonas on 2/19/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.TaxableServiceSummary')
        .controller('TaxableServiceSummaryUndeclaredCtrl',TaxableServiceSummaryUndeclaredCtrl);

    function TaxableServiceSummaryUndeclaredCtrl($scope, $state, $rootScope,TaxableServiceSummaryService){
        var vm = this;
        vm.companyID = "1";

        getSummaryHistory();

        function getSummaryHistory(){
            vm.companySummary = [];
            vm.serviceSummary = [];
            vm.summary = [];
            var flags = [];
            TaxableServiceSummaryService.getSummaryHistory().then(function(response){
                for(var i=0; i<response.data.length; i++){
                    if(response.data[i].company_id == vm.companyID && response.data[i].status == '0' && response.data[i].purchase_type == 'service'){
                        vm.companySummary.push(response.data[i]);
                    }
                }

                for(var i=0; i< vm.companySummary.length; i++){
                    vm.summary[i] = vm.companySummary[i].bill_date.split("-")[0] + "/" + vm.companySummary[i].bill_date.split("-")[1];
                    if( flags[vm.summary[i]]) continue;
                    flags[vm.summary[i]] = true;
                    vm.serviceSummary.push(vm.companySummary[i]);
                }
            });
        }

    }
})();