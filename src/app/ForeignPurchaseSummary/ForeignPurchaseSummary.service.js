/**
 * Created by Yonas on 2/19/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.ForeignPurchaseSummary')
        .service('ForeignPurchaseSummaryService', ForeignPurchaseSummaryService);
    /*@ngNoInject*/
    function ForeignPurchaseSummaryService(TokenRestangular, $rootScope) {

        var service = {
            getSummaryHistory: getSummaryHistory,
            getSummaryDetail: getSummaryDetail,
            createPurchaseSummary: createPurchaseSummary,
            editPurchaseSummary: editPurchaseSummary
        };
        return service;

        function getSummaryHistory(){
            debugger;
            return TokenRestangular.all('foreignpurchase').customGET('');
        }

        function getSummaryDetail(date,uid){
            debugger;
            return TokenRestangular.all('foreignpurchase/'+date+'/'+uid).customGET('');
        }

        function createPurchaseSummary(data){
            debugger;
            return TokenRestangular.all('foreignpurchase').customPOST(data);
        }

        function editPurchaseSummary(data){
            debugger;
            return TokenRestangular.all('foreignpurchase').customPUT(data);
        }
    }

})();

