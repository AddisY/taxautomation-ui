/**
 * Created by Yonas on 3/15/2017.
 */
(function(){
    'use strict';
    angular
        .module('app.bonus')
        .controller('BonusEditCtrl',BonusEditCtrl);

    function BonusEditCtrl($stateParams, $state, $rootScope,BonusService){
        var vm = this;
        vm.uid = $stateParams.uid;
        vm.date = $stateParams.date;
        vm.year = vm.date.split("-")[0];
        vm.month = vm.date.split("-")[1];
        vm.employeeList = [];
        vm.bonus = [];
        vm.editItems = [];
        vm.createItems = [];
        vm.editData = {};
        vm.createData = {};
        getEmployeeList();
        getBonusDetail();
        vm.addItem = addItem;
        vm.removeItem = removeItem;
        vm.editBonus = editBonus;


        function getEmployeeList(){
            debugger;
            BonusService.getEmployees(vm.uid).then(function(response){
                for(var i=0; i<response.employe_informations.length; i++){
                    vm.employeeList.push(response.employe_informations[i]);
                }
            });
        }

        function getBonusDetail(){
            BonusService.getBonus(vm.uid).then(function(response){
               for(var i=0; i<response.length;i++){
                   for(var j=0;j<response[i].bonus.length;j++){
                       if((response[i].bonus[j].pay_day.split("-")[0]+""+response[i].bonus[j].pay_day.split("-")[1]) == (vm.date.split("-")[0]+""+vm.date.split("-")[1])){
                           vm.bonus.push({
                               bonusID: response[i].bonus[j].id,
                               id: response[i].bonus[j].employe_id,
                               payday: response[i].bonus[j].pay_day.split("-")[2] * 1,
                               months: response[i].bonus[j].served_leaved_period.split("-")[0] * 1,
                               days:response[i].bonus[j].served_leaved_period.split("-")[1] * 1,
                               amount: response[i].bonus[j].total_amount * 1
                           })
                       }
                   }
               }
            });
        }

        function addItem(){
            vm.bonus.push({
                bonusID: "-",
                id: "",
                payday: "",
                months: "",
                days: "",
                amount: ""
            })
        }

        function removeItem(){
            var newDataList=[];
            vm.selectedAll = false;
            angular.forEach(vm.bonus, function(selected){
                if(!selected.selected){
                    newDataList.push(selected);
                }
            });
            vm.bonus = newDataList;
            vm.index = 0;
        }

        function editBonus(){
            for(var i=0; i<vm.bonus.length;i++){
                if(vm.bonus[i].bonusID != "-"){
                    vm.editItems.push({
                        id: vm.bonus[i].bonusID,
                        total_amount: vm.bonus[i].amount,
                        served_leaved_period: vm.bonus[i].months+"-"+vm.bonus[i].days,
                        pay_day: vm.year + "-" + vm.month+ "-" + vm.bonus[i].payday
                    })
                }
                else{
                   vm.createItems.push({
                       employe_id: vm.bonus[i].id,
                       pay_day: vm.year+"-"+vm.month+"-"+vm.bonus[i].payday,
                       served_leaved_period: vm.bonus[i].months+"-"+vm.bonus[i].days,
                       total_amount:vm.bonus[i].amount
                   })
                }
            }
            vm.editData = {bonus:vm.editItems};
            vm.createData = {bonus: vm.createItems};
            debugger;

            BonusService.editBonus(vm.editData).then(function(response){
            });
            BonusService.addBonus(vm.createData).then(function(respnse){
               $state.go('main.BonusUndeclared');
            });
        }
    }
})();