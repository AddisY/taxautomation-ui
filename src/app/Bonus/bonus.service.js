/**
 * Created by Yonas on 3/15/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.overtime')
        .service('BonusService', BonusService);
    /*@ngNoInject*/
    function BonusService(TokenRestangular, $rootScope) {

        var service = {
            getEmployees: getEmployees,
            getBonus: getBonus,
            getVariableBonus: getVariableBonus,
            addBonus: addBonus,
            editBonus: editBonus
        };

        return service;

        function getEmployees(companyID){
            debugger;
            return TokenRestangular.all('employebycompany/'+companyID).customGET('');
        }

        function getBonus(companyID){
            debugger;
            return TokenRestangular.all('bonusall/'+companyID).customGET('');
        }

        function getVariableBonus(uid,date){
            debugger;
            return TokenRestangular.all('bonusvariablebycompany/'+uid+"/"+date).customGET('');
        }

        function addBonus(data){
            debugger;
            return TokenRestangular.all('bonusstore').customPOST(data);
        }

        function editBonus(data){
            debugger;
            return TokenRestangular.all('bonusupdate').customPUT(data);
        }

    }

})();

